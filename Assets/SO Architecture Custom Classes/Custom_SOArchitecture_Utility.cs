﻿public static class Custom_SOArchitecture_Utility
{
    public const int ASSET_MENU_ORDER_VARIABLES = 121;
    public const int ASSET_MENU_ORDER_EVENTS = 122;
    public const int ASSET_MENU_ORDER_COLLECTIONS = 123;

    public const string VARIABLE_SUBMENU = "Custom Variables/";
    public const string COLLECTION_SUBMENU = "Custom Collections/";
}