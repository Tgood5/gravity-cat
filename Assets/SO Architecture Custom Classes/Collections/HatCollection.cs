using ScriptableObjectArchitecture;
using UnityEngine;

[CreateAssetMenu(
        fileName = "HatCollection.asset",
        menuName = Custom_SOArchitecture_Utility.COLLECTION_SUBMENU + "Hat",
        order = 120)]
public class HatCollection : ImmutableCollection<Hat>{}
