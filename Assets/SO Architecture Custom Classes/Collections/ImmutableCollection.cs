﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjectArchitecture
{
    //This class doesn't actually have an immutable list used internally. This class needs to be modified so that we can add items to the 
    //list via the unity editor but we'll get errors when we try to modify the list through code. This should be possible somehow..
    public abstract class ImmutableCollection<T> : SOArchitectureBaseObject, IEnumerable<T>
    {
        public T this[int index]
        {
            get
            {
                return _list[index];
            }
        }

        [SerializeField]
        private List<T> _list = new List<T>();

        public IList List
        {
            get
            {
                return _list;
            }
        }
        public Type Type
        {
            get
            {
                return typeof(T);
            }
        }

        public int Count { get { return List.Count; } }

        public bool Contains(T value)
        {
            return _list.Contains(value);
        }
        public int IndexOf(T value)
        {
            return _list.IndexOf(value);
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }
        public override string ToString()
        {
            return "Collection<" + typeof(T) + ">(" + Count + ")";
        }
        public T[] ToArray()
        {
            return _list.ToArray();
        }
    }
}
