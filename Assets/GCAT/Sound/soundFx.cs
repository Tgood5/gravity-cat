﻿public enum soundFx
{
    step,
    jump,
    rumble,
    impact,
    land,
    coin,

    shockedMeow,
    shortMeow,
    happyMeow,
    angryMeow,
    longMeow,
    talkingMeow,
    normalMeow,
    sadMeow,
    greetingMeow,

    purchase,
    clink,
}
