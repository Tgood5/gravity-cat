using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayList : MonoBehaviour
{
    public List<AudioClip> playList;
    public int index;

    public AudioClip GetCurrentClip() { return playList[index]; }
    public AudioClip GetNext()
    {
        index++;
        if(index >= playList.Count)
            index = 0;
        return GetCurrentClip();
    }

    // fisher yates shuffle
    public void Shuffle()
    {
        int n = playList.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n + 1);
            AudioClip toSwap = playList[k];
            playList[k] = playList[n];
            playList[n] = toSwap;
        }
        index = 0;
    }
}
