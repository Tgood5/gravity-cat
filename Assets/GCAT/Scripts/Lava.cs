﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    public float startLeft = -7;
    public float startWidth = 14;
    public float startTop = -6;
    public float startBottom = -10;

    public float noise = 0f;
    public float frequency = 1f;
    public float amplitude = 0f;
    public float speed = 0f;

    // const
    const float springconstant = 0.02f;
    const float damping = 0.04f;
    const float spread = 0.05f;
    const float z = -0.1f; // z offset (to display in foreground)

    // surface nodes
    float[] xPositions;
    float[] yPositions;
    float[] velocities;
    float[] accelerations;
    LineRenderer surface;

    int edgeCount;

    GameObject meshobject;
    Mesh mesh;
    GameObject[] colliders;

    // dimensions
    float baseheight;
    float left;
    float bottom;

     // inspector
    public GameObject splashPrefab;
    public Material mat;
    public GameObject lavaMesh;

    void Start()
    {
        SpawnLava(startLeft, startWidth, startTop, startBottom);
    }

    private void FixedUpdate()
    {
        ApplyWave();
        CalculatePhysics();
        UpdateMeshes();
    }

    void UpdateMeshes()
    {
        List<Vector3> vertices = new List<Vector3>();
        for (int i = 0; i < colliders.Length; i++)
        {
            vertices.Add(new Vector3(xPositions[i], yPositions[i], z));
            vertices.Add(new Vector3(xPositions[i + 1], yPositions[i + 1], z));
            vertices.Add(new Vector3(xPositions[i], bottom, z));
            vertices.Add(new Vector3(xPositions[i + 1], bottom, z));
        }
        mesh.SetVertices(vertices);
    }

    void ApplyWave()
    {
        for (int i = 0; i < xPositions.Length; i++) {
            yPositions[i] += amplitude * Mathf.Cos(i * 0.1f * frequency + Time.time);
        }
    }

    void CalculatePhysics()
    {
        // damping using Euler method
        for (int i = 0; i < xPositions.Length; i++)
        {
            float force = springconstant * (yPositions[i] - baseheight) + velocities[i] * damping;
            accelerations[i] = -force; // -force/mass
            yPositions[i] += velocities[i];
            velocities[i] += accelerations[i];
            surface.SetPosition(i, new Vector3(xPositions[i], yPositions[i], z));
        }

        // propagation
        float[] leftDeltas = new float[xPositions.Length];
        float[] rightDeltas = new float[xPositions.Length];

        for (int j = 0; j < 8; j++)
        {
            for (int i = 0; i < xPositions.Length; i++)
            {
                if (i > 0)
                {
                    leftDeltas[i] = spread * (yPositions[i] - yPositions[i - 1]);
                    velocities[i - 1] += leftDeltas[i];
                }
                if (i < xPositions.Length - 1)
                {
                    rightDeltas[i] = spread * (yPositions[i] - yPositions[i + 1]);
                    velocities[i + 1] += rightDeltas[i];
                }
            }
        }

        // apply new height data
        for (int i = 0; i < xPositions.Length; i++)
        {
            if (i > 0)
            {
                yPositions[i - 1] += leftDeltas[i];
            }
            if (i < xPositions.Length - 1)
            {
                yPositions[i + 1] += rightDeltas[i];
            }
        }
    }

    public void Splash(float xpos, float velocity)
    {
        if (xpos >= xPositions[0] && xpos <= xPositions[xPositions.Length - 1])
        {
            xpos -= xPositions[0];
            int index = Mathf.RoundToInt((xPositions.Length - 1) * (xpos / (xPositions[xPositions.Length - 1] - xPositions[0])));
            velocities[index] = velocity;

            float lifetime = 0.93f + Mathf.Abs(velocity) * 0.07f;
            var system = splashPrefab.GetComponent<ParticleSystem>().main;

            system.startSpeed = 8 + 2 * Mathf.Pow(Mathf.Abs(velocity), 0.5f);
            system.startSpeed = 9 + 2 * Mathf.Pow(Mathf.Abs(velocity), 0.5f);
            system.startLifetime = lifetime;

            Vector3 position = new Vector3(xPositions[index], yPositions[index], transform.position.z);
            Quaternion rotation = Quaternion.LookRotation(new Vector3(xPositions[Mathf.FloorToInt(xPositions.Length / 2)], baseheight + 8, 5) - position);

            GameObject splish = Instantiate(splashPrefab, transform.position + position, rotation) as GameObject;
            Destroy(splish, lifetime + 0.3f);
        }
    }

    public void SpawnLava(float Left, float Width, float Top, float Bottom)
    {
        // calculate number of nodes needed
        edgeCount = Mathf.RoundToInt(Width) * 5; // 5 nodes per unit width
        int nodecount = edgeCount + 1;

        // render the surface line
        surface = gameObject.AddComponent<LineRenderer>();
        surface.material = mat;
        surface.material.renderQueue = 1000;
        surface.positionCount = nodecount;
        surface.startWidth = 0.1f;
        surface.endWidth = 0.1f;
        surface.useWorldSpace = false;
        surface.sortingLayerName = "Level";

        // init vars and arrays
        xPositions = new float[nodecount];
        yPositions = new float[nodecount];
        velocities = new float[nodecount];
        accelerations = new float[nodecount];
      
        baseheight = Top;
        bottom = Bottom;
        left = Left;

        // set array values
        for (int i = 0; i < nodecount; i++)
        {
            yPositions[i] = Top;
            xPositions[i] = Left + Width * i / edgeCount;
            accelerations[i] = 0;
            velocities[i] = 0;
            surface.SetPosition(i, new Vector3(xPositions[i], yPositions[i], z));
        }

        // Building the mesh   
        //meshobject = new GameObject();
        mesh = new Mesh();
        colliders = new GameObject[edgeCount];
        List<Vector3> vertices = new List<Vector3>();
        List<int> tris = new List<int>();
        List<Vector2> uvs = new List<Vector2>();

        // for each edge segment
        for(int i=0; i<edgeCount; i++)
        {
            // add vertices
            vertices.Add(new Vector3(xPositions[i], yPositions[i], z));
            vertices.Add(new Vector3(xPositions[i + 1], yPositions[i + 1], z));
            vertices.Add(new Vector3(xPositions[i], bottom, z));
            vertices.Add(new Vector3(xPositions[i + 1], bottom, z));

            // add uvs
            uvs.Add(new Vector2(0, 1));
            uvs.Add(new Vector2(1, 1));
            uvs.Add(new Vector2(0, 0));
            uvs.Add(new Vector2(1, 0));

            // add triangles
            int baseIndex = vertices.Count - 4;
            int[] Tris = new int[6] { baseIndex, baseIndex + 1, baseIndex + 3, baseIndex + 3, baseIndex + 2, baseIndex };
            tris.AddRange(Tris);

            // create collider
            colliders[i] = new GameObject();
            colliders[i].name = "Trigger";
            colliders[i].AddComponent<BoxCollider2D>();
            colliders[i].transform.parent = transform;
            colliders[i].transform.position = new Vector3(Left + Width * (i + 0.5f) / edgeCount, Top - 0.5f, 0);
            colliders[i].transform.localScale = new Vector3(Width / edgeCount, 1, 1);
            colliders[i].GetComponent<BoxCollider2D>().isTrigger = true;
            colliders[i].AddComponent<LavaDetector>();
        }
     
        mesh.SetVertices(vertices);
        mesh.SetUVs(0, uvs);;
        mesh.SetTriangles(tris, 0);
        mesh.RecalculateBounds();

        // create gameobject
        meshobject = Instantiate(lavaMesh, Vector3.zero, Quaternion.identity) as GameObject;
        meshobject.GetComponent<MeshFilter>().mesh = mesh;
        meshobject.transform.SetParent(transform);       
    }

    class LavaDetector : MonoBehaviour
    {
        void OnTriggerEnter2D(Collider2D Hit)
        {
            Rigidbody2D hit = Hit.GetComponent<Rigidbody2D>();
            if (hit != null)
                transform.GetComponentInParent<Lava>().Splash(transform.position.x, hit.velocity.y * hit.mass / 40f);
        }
    }
}
