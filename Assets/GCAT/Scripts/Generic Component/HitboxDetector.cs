﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxDetector : MonoBehaviour
{
    public LayerMask groundLayer;
    public bool grounded;
    public bool onWall;
    public bool onLeftWall;
    public bool onRightWall;
    public bool onCeiling;
    public Collider2D top;
    public Collider2D bottom;
    public Collider2D left;
    public Collider2D right;

    public Collider2D topHit;
    public Collider2D botHit;
    public Collider2D leftHit;
    public Collider2D rightHit;

    public int side;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.IsTouching(top))
            topHit = collision;

        if (collision.IsTouching(bottom))
        {
            botHit = collision;
            transform.SetParent(collision.gameObject.transform);
        }

        if (collision.IsTouching(right))
            rightHit = collision;
        if (collision.IsTouching(left))
            leftHit = collision;

        UpdateValues();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision == topHit)
            topHit = null;

        if (collision == botHit)
        {
            botHit = null;
            transform.SetParent(null);
        }

        if (collision == rightHit)
            rightHit = null;
        if (collision == leftHit)
            leftHit = null;

        UpdateValues();
    }

    private void UpdateValues()
    {
        onCeiling = topHit != null;
        grounded = botHit != null;
        onLeftWall = leftHit != null;
        onRightWall = rightHit != null;

        onWall = onLeftWall || onRightWall;
        side = onRightWall ? 1 : -1;
    }
}
