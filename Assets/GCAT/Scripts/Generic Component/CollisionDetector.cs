﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
    public LayerMask groundLayer;
    public bool grounded;
    public bool onWall;
    public bool onLeftWall;
    public bool onRightWall;
    public bool onCeiling;
    public float collisionRadius;
    public float groundOffset;
    public float groundRayLength = 0.6f;
    public Vector2 leftOffset;
    public Vector2 rightOffset;
    public float topOffset;

    public Color gizmoColor = Color.red;
    public int side;

    void Update()
    {
        // detect collisions at offset points
        RaycastHit2D groundHitLeft;
        RaycastHit2D groundHitRight;
        Vector2 offset = transform.up * groundOffset;
        Vector2 pos = (Vector2)transform.position;
        groundHitLeft = Physics2D.Raycast(pos - offset, -transform.right, groundRayLength, groundLayer);
        groundHitRight = Physics2D.Raycast(pos - offset, transform.right, groundRayLength, groundLayer);
        grounded = groundHitLeft || groundHitRight;
        if (groundHitLeft)
            transform.SetParent(groundHitLeft.collider.transform);
        else if (groundHitRight)
            transform.SetParent(groundHitRight.collider.transform);
        else
            transform.SetParent(null);
        onCeiling = Physics2D.OverlapCircle(transform.position + topOffset*transform.up, collisionRadius, groundLayer);
        //onLeftWall = Physics2D.OverlapCircle((Vector2)transform.position + leftOffset, collisionRadius, groundLayer);
        //onRightWall = Physics2D.OverlapCircle((Vector2)transform.position + rightOffset, collisionRadius, groundLayer);
        //onWall = onLeftWall || onRightWall;
        //side = onRightWall ? 1 : -1;
    }

    private void OnDrawGizmos()
    {
        //Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = gizmoColor;
        //Gizmos.DrawWireSphere((Vector2)transform.position + leftOffset, collisionRadius);
        //Gizmos.DrawWireSphere((Vector2)transform.position + rightOffset, collisionRadius);
        Gizmos.DrawWireSphere(transform.position + (topOffset * transform.up), collisionRadius);

        Vector3 offset = transform.up * groundOffset;
        Gizmos.DrawLine(transform.position - offset, transform.position - offset + (transform.right * groundRayLength));
        Gizmos.DrawLine(transform.position - offset, transform.position - offset - (transform.right * groundRayLength));
    }
}
