﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWrapper : MonoBehaviour
{
    [SerializeField] bool DoWrapping;   // toggle behavior
    bool isWrapping;   // internal use only
    Camera cam;
    public BoxCollider2D screenArea;
    public SpriteRenderer cloneSprite;

    [SerializeField] Transform anchor = null;
    [SerializeField] Transform leftClone = null;
    [SerializeField] Transform rightClone = null;
    [SerializeField] Transform followPoint = null;

    private void OnDestroy()
    {
        if(anchor)
            Destroy(anchor.gameObject);
    }
    private void Start()
    {
        GenerateClones();
        anchor.SetParent(null);
        cam = Camera.main;
        screenArea = GameObject.FindWithTag("PlayArea").GetComponent<BoxCollider2D>();
        if (screenArea != null)
            AdjustClonesToArea();
        else
            AdjustClonesToCamera();
        ToggleWrapping(DoWrapping);
    }

    void Update()
    {
        if(DoWrapping)
        {
            anchor.position = followPoint.position;
            leftClone.transform.rotation = transform.rotation;
            rightClone.transform.rotation = transform.rotation;
            if (screenArea != null)
                AreaWrap();
            else
                ScreenWrap();
        }                 
    }

    void AreaWrap()
    {
        Vector3 newPos = transform.position;
        if (newPos.x < screenArea.bounds.min.x && !isWrapping)
        {
            isWrapping = true;
            newPos.x += screenArea.bounds.size.x;
            transform.position = newPos;
        }
        else if (newPos.x > screenArea.bounds.max.x && !isWrapping)
        {
            isWrapping = true;
            newPos.x -= screenArea.bounds.size.x;
            transform.position = newPos;
        }
        else
        {
            isWrapping = false;
        }

    }

    void ScreenWrap()
    {
        Vector3 newPos = transform.position;
        Vector3 pos = cam.WorldToViewportPoint(transform.position);
        if ((pos.x < 0 || pos.x > 1) && !isWrapping)
        {
            isWrapping = true;
            newPos.x = -newPos.x * 0.99f;
            transform.position = newPos;
        }
        else
        {
            isWrapping = false;
        }         
    }

    void GenerateClones()
    {
        if(cloneSprite == null)
        {
            cloneSprite = GetComponent<SpriteRenderer>();
        }
        if (anchor == null)
        {
            anchor = new GameObject().GetComponent<Transform>();
            anchor.name = "Clone Anchor";
        }
            
        if(leftClone == null)
        {
            leftClone = new GameObject().GetComponent<Transform>();
            leftClone.transform.SetParent(anchor);
            leftClone.name = cloneSprite.name + " left clone";
            SpriteRenderer sr = leftClone.gameObject.AddComponent<SpriteRenderer>();
            sr.sprite = cloneSprite.sprite;
            sr.sortingLayerID = cloneSprite.sortingLayerID;
            sr.sortingOrder = cloneSprite.sortingOrder;
        }
        if (rightClone == null)
        {
            rightClone = new GameObject().GetComponent<Transform>();
            rightClone.transform.SetParent(anchor);
            rightClone.name = cloneSprite.name + " right clone";
            SpriteRenderer sr = rightClone.gameObject.AddComponent<SpriteRenderer>();
            sr.sprite = cloneSprite.sprite;
            sr.sortingLayerID = cloneSprite.sortingLayerID;
            sr.sortingOrder = cloneSprite.sortingOrder;
        }

        if (followPoint == null)
        {
            followPoint = transform;
        }

    }
    public void AdjustClonesToCamera()
    {
        if (!leftClone || !rightClone)
            return;

        //calculate where perspective field of view intersects play area
        //var screenBottomLeft = cam.ViewportToWorldPoint(new Vector3(0, 0, transform.position.z));
        //var screenTopRight = cam.ViewportToWorldPoint(new Vector3(1, 1, transform.position.z));
        //float screenWidth = screenTopRight.x - screenBottomLeft.x;
        //float screenHeight = screenTopRight.y - screenBottomLeft.y;

        float screenHeight = 2.0f * cam.transform.position.z * Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);
        float screenWidth = screenHeight * cam.aspect;

        leftClone.localPosition = Vector3.zero;
        leftClone.Translate(-screenWidth, 0, 0);

        rightClone.localPosition = Vector3.zero;
        rightClone.Translate(screenWidth, 0, 0);
    }

    public void AdjustClonesToArea()
    {
        if (!leftClone || !rightClone)
            return;

        leftClone.localPosition = Vector3.zero;
        leftClone.Translate(-screenArea.bounds.size.x, 0, 0);

        rightClone.localPosition = Vector3.zero;
        rightClone.Translate(screenArea.bounds.size.x, 0, 0);
    }

    public void ToggleWrapping(bool toggleOn)
    {
        anchor.gameObject.SetActive(toggleOn);
        DoWrapping = toggleOn;
        
    }
}
