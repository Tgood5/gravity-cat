﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySource : MonoBehaviour
{
    public float gravityStrength = 9.81f;
    public Collider2D range;
    public int priority = 0; // higher value source will override gravity from lower value sources

    public void Start()
    {
        range = GetComponent<Collider2D>();
    }

    public virtual Vector2 GetGravity(Vector3 position)
    {
        return Physics2D.gravity;
    }

    public virtual float GetMagnitude(Vector3 position)
    {
        return gravityStrength / Vector2.Distance(transform.position, position);
    }
}
