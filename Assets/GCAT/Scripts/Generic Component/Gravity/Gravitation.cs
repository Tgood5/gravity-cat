﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// will handle an objects gravitational pull in regards to the gravity sources active upon it
[RequireComponent(typeof(Rigidbody2D))]
public class Gravitation : MonoBehaviour
{
    // set in inspector
    [SerializeField] Rigidbody2D body;

    public float rotationSpeed = 10f;
    [SerializeField] List<GravitySource> gravitySources = new List<GravitySource>();

    GravitySource prev;
    float bufferTimerVal;
    static float MAX_BUFFER_TIME = 0.22f;

    private void Awake()
    {
        if (body == null)
            body = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        // apply gravity force to rigidbody
        Vector2 grav = GetGravitation() * body.gravityScale;
        body.AddForce(grav);      
    }

    private void Update()
    {
        if (body.simulated && gravitySources.Count > 0)
        {
            // Rotate based on gravity axis
            float offset = 90f;
            Vector2 dir = -GetMainUpAxis();
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(angle + offset, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);


            if (prev != gravitySources[0])
                if (bufferTimerVal <= 0f)
                {
                    prev = gravitySources[0];
                    bufferTimerVal = MAX_BUFFER_TIME;
                }
        }
     
        bufferTimerVal -= Time.deltaTime;      
    }

    // only use gravity from single source
    public Vector2 GetGravitation()
    {
        Vector2 gravity = Vector2.zero;      
        //if (gravitySources.Count < 1)
        if(prev == null)
            return gravity;
        //int priority = gravitySources[0].priority;
        //gravity += gravitySources[0].GetGravity(transform.position);
        gravity += prev.GetGravity(transform.position);
        /*
        for (int i = 0; i < gravitySources.Count; i++)
        {
            if (gravitySources[i].priority < priority)
                break;  // stop adding when reaching a lower priority source
            gravity += gravitySources[i].GetGravity(transform.position);
        }   
        */

        return gravity;      
    }

    public Vector2 GetGravitation(out Vector3 upAxis)
    {
        Vector2 gravity = Vector2.zero;          

        for (int i = 0; i < gravitySources.Count; i++)
            gravity += gravitySources[i].GetGravity(transform.position);
        
        upAxis = -gravity.normalized;
        return gravity;
    }

    // get up axis from all gravity
    public Vector2 GetUpAxis()
    {
        Vector2 g = Vector2.zero;
        for (int i = 0; i < gravitySources.Count; i++)
            g += gravitySources[i].GetGravity(transform.position);
        
        return -g.normalized;
    }

    // get up axis from strongest source only 
    // ** CURRENTLY 
    public Vector2 GetMainUpAxis()
    {
        Vector2 g = Vector2.zero;
        //float m = 0;
        //if (gravitySources.Count >= 1)
        if(prev != null)
            g += prev.GetGravity(transform.position);
        /*
        for (int i = 0; i < gravitySources.Count; i++)
        {
            float iMag = gravitySources[i].GetMagnitude(transform.position);
            if (iMag > m)
            {
                m = iMag;
                g = gravitySources[i].GetGravity(transform.position);
            }
        }     
        */

        return -g.normalized;
    }

    // add in order of priority, then in order of strength
    public void AddSource(GravitySource source)
    {
        for(int i=0; i< gravitySources.Count; i++)
        {
            if (source.priority > gravitySources[i].priority)
            {
                gravitySources.Insert(i, source);
                return;
            }  
            else if(source.priority == gravitySources[i].priority)
            {
                if (source.gravityStrength > gravitySources[i].gravityStrength)
                    gravitySources.Insert(i, source);
                else
                    gravitySources.Insert(i+1, source);
                return;
            }
        }
        gravitySources.Add(source);
    }

    public void RemoveSource(GravitySource source)
    {
        gravitySources.Remove(source);
    }
}
