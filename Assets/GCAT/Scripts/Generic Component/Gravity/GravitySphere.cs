﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class GravitySphere : GravitySource
{
    CircleCollider2D circ { get { return range as CircleCollider2D; } }

    // gravity outside this radius will start being affected by distance from source
    [Range(0f, 1.0f)]
    public float dropOff = 0.5f;
    float dropRadius { get { return circ.radius * dropOff; } }
    public override Vector2 GetGravity(Vector3 position)
    {
        if(Vector2.Distance(transform.position, position) > dropRadius)
            return (transform.position - position).normalized * GetMagnitude(position);
        else
            return (transform.position - position).normalized * gravityStrength;
    }
    public override float GetMagnitude(Vector3 position)
    {
        return gravityStrength / (Vector2.Distance(transform.position, position));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Gravitation grav = collision.gameObject.GetComponent<Gravitation>();
        if (grav != null)
            grav.AddSource(this);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Gravitation grav = collision.gameObject.GetComponent<Gravitation>();
        if (grav != null)
            grav.RemoveSource(this);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, circ.radius * dropOff);
    }
}
