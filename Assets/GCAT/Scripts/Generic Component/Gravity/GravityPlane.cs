﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// gravity source that pulls in a single direction
public class GravityPlane : GravitySource
{
    public override Vector2 GetGravity(Vector3 position)
    {
        Vector2 up = transform.up; // pull opposite to transform up
        return -gravityStrength * up;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Gravitation grav = collision.gameObject.GetComponent<Gravitation>();
        if (grav != null)
            grav.AddSource(this);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Gravitation grav = collision.gameObject.GetComponent<Gravitation>();
        if (grav != null)
            grav.RemoveSource(this);
    }

}
