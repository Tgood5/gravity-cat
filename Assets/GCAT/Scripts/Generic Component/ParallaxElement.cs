﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxElement : MonoBehaviour
{
    public Transform mainCam;
    public float relativeX = 0.5f;
    public float relativeY = 1f;

    void Start()
    {
        if (mainCam == null)
            mainCam = Camera.main.transform;
    }

    void Update()
    {
        transform.position = new Vector3(mainCam.position.x * relativeX, mainCam.position.y * relativeY, transform.position.z);
    }
}
