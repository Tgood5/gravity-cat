﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShaker : MonoBehaviour
{
    public const string shakeNotification = "cameraShaker.shakeNotification";

    [SerializeField]CinemachineVirtualCamera shakeCam;
    public static CinemachineBasicMultiChannelPerlin shaker { get; set; }

    void Start()
    {
        //shakeCam = GetComponent<CinemachineVirtualCamera>();
        shaker = shakeCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        this.AddObserver(OnShakeNotification, shakeNotification);
    }
    private void OnDestroy()
    {
        this.RemoveObserver(OnShakeNotification, shakeNotification);
    }

    void OnShakeNotification(object sender, object args)
    {
        //todo

    }
}
