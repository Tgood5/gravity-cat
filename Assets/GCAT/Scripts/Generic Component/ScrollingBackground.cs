﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    Material material;
    Vector2 offset;

    public float xSpeed, ySpeed;

    private void Awake()
    {
        material = GetComponent<Renderer>().material;
    }

    void Start()
    {
        offset = new Vector2(xSpeed, ySpeed);
    }

    void Update()
    {
        material.mainTextureOffset += offset * Time.deltaTime;
    }
}
