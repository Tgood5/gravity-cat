﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFxSource : MonoBehaviour
{
    public SoundFxPlayerList playList;
    public AudioSource player;
    //TODO volume, pitchrange

    public void PlaySound()
    {
        if (SoundManager.sfxOn)
        {
            player.clip = playList.GetSound();
            player.Play();
        }      
    }
}
