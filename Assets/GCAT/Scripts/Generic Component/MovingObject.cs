﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Experimental.GraphView.GraphView;
using UnityEngine.UI;

public class MovingObject : MonoBehaviour
{
    public Vector3 direction;
    public float speed = 1f;
    // optional SO reference will override speed
    public BaseVariable<float> speedReference;

    // if greater than 0, object will reverse direction after moving this far
    public float returnDist = 0.0f;
    // track total distance moved in a single direction
    float traveled = 0.0f;

    public bool bounceOnCollisionForTroy = false;
    public LayerMask bounceLayers;

    public Rigidbody2D body;

    void FixedUpdate()
    {
        float dist;
        if(speedReference != null)
            dist = speedReference.value * Time.deltaTime;
        else
            dist = speed * Time.deltaTime;

        if (body)
            body.MovePosition(transform.position + direction*dist);
        else
            transform.Translate(direction * dist);

        traveled += dist;
        if(returnDist > 0.0f && traveled > returnDist)
        {
            traveled = 0.0f;
            direction *= -1;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // check that collision layer is a bounce layer
        if (bounceOnCollisionForTroy && bounceLayers.Contains(collision.gameObject.layer))
        {
            Vector2 newvec = collision.GetContact(0).normal * -1;
            newvec.x = Mathf.RoundToInt(newvec.x);
            newvec.y = Mathf.RoundToInt(newvec.y);
            if (newvec.x == direction.x) direction.x *= -1;
            if (newvec.y == direction.y) direction.y *= -1;
            Debug.Log(name + " direction " + direction + " , contact normal " + newvec.x + "."+ newvec.y);
        }
    }
}
