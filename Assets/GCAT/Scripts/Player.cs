﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] bool playerHasControl = true;

    // notification
    public const string gameOverNotification = "player.gameover";

    [Header("Set in Inspector")] // set un inspector prefab
    public Animator animator;
    public Rigidbody2D body;
    public Transform sprite;    // for jumpsqueeze reference
    public Transform modelAnchor;   // for flip facing reference
    public CollisionDetector detector;
    public Gravitation gravitation;
    public ScreenWrapper wrapper;
    public ParticleSystem runDust;
    public ParticleSystem jumpDust;
    public ParticleSystem trailDust;
    public Ghoster ghostTrail;
    public SpriteRenderer hatSprite;
    public SoundFxSource shockedMeowFx;
    public SoundFxSource shortMeowFx;
    public Collider2D collider2d;

    // input
    [SerializeField] Vector2 direction = Vector2.zero;
    [SerializeField] bool facingRight = true;
    [SerializeField] bool grounded = false;
    public bool sprinting;
    public bool initDash = true;
    public bool jumpPressed = false;

    [Header("Horizontal Movement")]
    public float currentSpeed;
    public float runSpeed = 8f;
    public float walkSpeed = 5f;
    public float crawlSpeed = 2f;
    public float acceleration = 10f;
    
    [Header("Jumping")]    
    public float jumpSpeed = 16f;
    public int maxAirJumps;
    public int remainingJumps;   
    public const float BufferWindow = 0.2f;
    public float jumpBuffer { get; private set; }
    public float coyoteTime { get; private set; }                   // short window where player can still jump after walking off a platform

    [Header("Physics")]
    public float gravity = 1f;
    public float linearDrag = 4f;
    public float fallMultiplier = 8f;           // increased fall gravity multiplier after reaching peak jump height 5
    public float shortHopFallMultiplier = 4f;   // increased fall gravity multiplier after releasing jump key early 2
    
    // particle dust  
    private ParticleSystem.EmissionModule emission;
    
    void Start()
    {
        emission = runDust.emission;
        ghostTrail.enabled = false;
        hatSprite.sprite = CollectionManager.GetHatSprite();
        SetFacing(facingRight);
    }

    private void Update()
    {
        // DETECTOR
        if (detector.enabled)
        {
            // collision check
            bool wasOnGround = grounded;
            grounded = detector.grounded;

            // check crushed
            if (grounded && detector.onCeiling)
                PlayerHurt();

            // on landing
            if (!wasOnGround && grounded)
            {
                initDash = true;
                jumpDust.Play();
                SoundManager.PlaySound(soundFx.land);
                StartCoroutine(JumpSqueeze(1.3f, 0.7f, 0.1f));
            }           

            // Animation
            animator.SetBool("grounded", grounded);
        }

        // PLAYER CONTROL
        jumpBuffer -= Time.deltaTime;      
        if (playerHasControl)
        {
            // sprint check
            if (Input.GetButton("Sprint") && grounded)
            {
                sprinting = true;
                ghostTrail.enabled = true;
            }
            else if (grounded)
            {
                sprinting = false;
                ghostTrail.enabled = false;
            }

            // jump check 
            //jumpPressed = false;
            if(jumpBuffer < 0)
                jumpPressed = false;
            if (Input.GetButtonDown("Jump"))
            {
                if(jumpBuffer < 0)
                {
                    jumpPressed = true;
                    jumpBuffer = BufferWindow;
                }                
            }
                              
            // movement check
            direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        }
       
    }
    
    void FixedUpdate()
    {
        // apply physics 
        HorizontalMove();
        VerticalMove();
        modifyPhysics();       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // player is defeated
        if (collision.CompareTag("harmful"))
        {
            PlayerHurt();
        }            
    }

    void HorizontalMove()
    {
        float topSpeed = walkSpeed;
        if (sprinting)
            topSpeed = runSpeed;
        float xDir = Mathf.RoundToInt(direction.x);

        // add horizontal force
        body.AddForce(transform.right * xDir * acceleration, ForceMode2D.Impulse);
        // push down against rounded surface
        if (grounded && jumpBuffer < 0)
            body.AddForce(-transform.up * acceleration, ForceMode2D.Impulse);
            
        if ((xDir > 0 && !facingRight) || (xDir < 0 && facingRight))
            FlipFacing();

        // limit velocity
        // get direction relative to rotation
        Vector2 localDir = transform.InverseTransformDirection(body.velocity);

        if (Mathf.Abs(localDir.x) > topSpeed)
            localDir.x = Mathf.Sign(localDir.x) * topSpeed;

        body.velocity = transform.TransformDirection(localDir);

        currentSpeed = Mathf.Abs(localDir.x);
        animator.SetFloat("xSpeed", currentSpeed);
        animator.SetFloat("ySpeed", localDir.y);

        // particle trail
        if (xDir != 0)
            emission.rateOverTime = 35f;
        else
            emission.rateOverTime = 0;
    }

    void VerticalMove()
    {
        // grounded
        if (grounded && jumpBuffer < 0)
        {
            remainingJumps = maxAirJumps;
            coyoteTime = BufferWindow;           
        }
        else
        {
            coyoteTime -= Time.deltaTime;
            emission.rateOverTime = 0;
        }

        // jump
        if ((jumpPressed) && (coyoteTime > 0 || remainingJumps > 0))
        {
            if (!grounded)
                remainingJumps--;
            //jumpBuffer = BufferWindow;
            jumpPressed = false;
            coyoteTime = 0;
            body.drag = linearDrag * 0.15f;
            body.gravityScale = 0;
            body.velocity = new Vector2(body.velocity.x, 0); // reset vertical velocity           
            body.AddForce(transform.up * jumpSpeed, ForceMode2D.Impulse);
            // extra effects
            jumpDust.Play();
            trailDust.Play();
            SoundManager.PlaySound(soundFx.jump);
            StartCoroutine(JumpSqueeze(0.7f, 1.3f, 0.1f));
            if(sprinting)
                shortMeowFx.PlaySound();
        }
    }

    void modifyPhysics()
    {
        // get direction relative to rotation
        Vector2 localDir = transform.InverseTransformDirection(body.velocity);

        if (grounded && jumpBuffer < 0)
        {
            body.drag = linearDrag;
            body.gravityScale = 0;
        }
        else
        {
            // Jump Arc
            body.drag = linearDrag * 0.15f;
            body.gravityScale = gravity;
                     
            if (localDir.y < 0 )
                body.gravityScale = gravity * fallMultiplier;
            else if (localDir.y > 0 && !Input.GetButton("Jump") && playerHasControl)
                body.gravityScale = gravity * (shortHopFallMultiplier);
        }
    }
    // use to jump without player input
    public void Jump()
    {
        jumpPressed = true;
    }
    // use to move without reading input (cutscene)
    public void SetDirectionManual(Vector2 newDir)
    {
        direction = newDir;
    }
    public void SetSprintState(bool isSprinting)
    {
        sprinting = isSprinting;
    }
    void FlipFacing()
    {
        facingRight = !facingRight;
        SetFacing(facingRight);
    }
    void SetFacing(bool faceRight)
    {
        modelAnchor.localRotation = Quaternion.Euler(0, facingRight ? 0 : 180, 0);
        if (grounded)
            animator.SetTrigger("turn");
    }
    IEnumerator JumpSqueeze(float xSqueeze, float ySqueeze, float seconds)
    {
        Vector3 originalSize = Vector3.one;
        Vector3 newSize = new Vector3(xSqueeze, ySqueeze, originalSize.z);
        float t = 0f;
        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            sprite.transform.localScale = Vector3.Lerp(originalSize, newSize, t);
            yield return null;
        }
        t = 0f;
        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            sprite.transform.localScale = Vector3.Lerp(newSize, originalSize, t);
            yield return null;
        }
        sprite.transform.localScale = originalSize;
    }

    public void PlayerHurt()
    {
        this.PostNotification(gameOverNotification);
        playerHasControl = false;
        animator.SetBool("shocked", true);
        shockedMeowFx.PlaySound();
        
        transform.Translate(0, 0, -.2f, Space.World);
        detector.enabled = false;
        collider2d.enabled = false;
        body.velocity = Vector2.zero;
        grounded = false;
        body.AddForce(Vector2.up * jumpSpeed * 1.5f, ForceMode2D.Impulse);        
    }

    public void ActivatePlayerControl()
    {
        playerHasControl = true;
        collider2d.enabled = true;
        detector.enabled = true;
        gravitation.enabled = true;
    }

    public void ActivatePlayerScreenWrap()
    {
        wrapper.ToggleWrapping(true);
    }
}
