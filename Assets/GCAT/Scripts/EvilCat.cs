﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvilCat : MonoBehaviour
{
    public const string IDLE_ANIM = "idle";
    public const string LAUGH_ANIM = "laugh";
    public const string ANGRY_ANIM = "angry";

    float moveSpeed = 10f;
    float rotSpeed = 5f;
    float maxAngle = 15;

    [SerializeField] Quaternion neutral;
    [SerializeField] Quaternion rightTilt;
    [SerializeField] Quaternion leftTilt;

    // inspector
    [SerializeField] Animator saucerAnim = null;
    [SerializeField] Animator catAnim = null;
    [SerializeField] Transform modelAnchor = null;
    [SerializeField] Transform seating = null;
    [SerializeField] ParticleSystem exhaustFX;
    [SerializeField] ParticleSystem beamFX;

    [SerializeField] Transform lookAt;
    [SerializeField] Transform followPoint;
    [SerializeField] bool isFollowing;

    public Transform beamAnchor;

    private void Update()
    {
        if (lookAt != null)
        {
            bool faceRight = transform.position.x < lookAt.position.x;
            seating.localRotation = Quaternion.Euler(0, faceRight ? 0 : 180, modelAnchor.localRotation.z);
        }

        if (isFollowing)
        {
            
            float relativeX = transform.position.x - followPoint.position.x;
            if (relativeX < -1.5)
                modelAnchor.localRotation = Quaternion.Slerp(modelAnchor.localRotation, Quaternion.Euler(0, transform.rotation.y, -maxAngle), Time.deltaTime * rotSpeed);
            else if (relativeX > 1.5)
                modelAnchor.localRotation = Quaternion.Slerp(modelAnchor.localRotation, Quaternion.Euler(0, transform.rotation.y, maxAngle), Time.deltaTime * rotSpeed);
            else
                modelAnchor.localRotation = Quaternion.Slerp(modelAnchor.localRotation, Quaternion.Euler(0, transform.rotation.y, 0), Time.deltaTime * rotSpeed);

            transform.position = Vector3.MoveTowards(transform.position, followPoint.position, Time.deltaTime * moveSpeed);// Vector3.Lerp(transform.position, followPoint.position, Time.deltaTime * moveSpeed);          
        }
        
    }

    public void TriggerCatAnim(string anim)
    {
        catAnim.SetTrigger(anim);
    }
    public void TriggerSaucerAnim(string anim)
    {
        saucerAnim.SetTrigger(anim);
    }

    public void SetFollowTransform(Transform toFollow)
    {
        followPoint = toFollow;
    }
    public void SetFollowing(bool isFollowing)
    {
        this.isFollowing = isFollowing;
    }
    // transform for cat to face towards
    public void SetFocusTransform(Transform toLookAt)
    {
        lookAt = toLookAt;
    }
    // check if saucer reached destination
    public bool IsNearFollowPoint()
    {
        if (followPoint == null) return true;
        return Mathf.Abs(Vector2.Distance(transform.position, followPoint.position)) < 1;
    }

    public void ToggleBeam(bool toggleOn)
    {
        if (toggleOn) beamFX.Play();
        else beamFX.Stop();
    }
}
