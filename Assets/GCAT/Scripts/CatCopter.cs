﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatCopter : MonoBehaviour
{
    [SerializeField] float moveSpeed = 2f;
    [SerializeField] float rotSpeed = 5f;
    [SerializeField] float maxAngle = 15;
    [SerializeField] float rotBufferdist = .2f; //distance needed before rotation begins

    //[SerializeField] Quaternion neutral;
    //[SerializeField] Quaternion rightTilt;
    //[SerializeField] Quaternion leftTilt;

    // inspector
    [SerializeField] Animator vehicleAnim = null;
    //[SerializeField] Animator riderAnim = null;
    [SerializeField] Transform modelAnchor = null;
    //[SerializeField] Transform seating = null;

    public Transform lookAt;
    public Transform followPoint;
    public bool isFollowing;

    private void Update()
    {
        if (lookAt != null)
        {
            //bool faceRight = transform.position.x < lookAt.position.x;
            //seating.localRotation = Quaternion.Euler(0, faceRight ? 0 : 180, modelAnchor.localRotation.z);
        }

        if (isFollowing)
        {
            
            float relativeX = transform.position.x - followPoint.position.x;
            if (relativeX < -rotBufferdist)
                modelAnchor.localRotation = Quaternion.Slerp(modelAnchor.localRotation, Quaternion.Euler(0, transform.rotation.y, -maxAngle), Time.deltaTime * rotSpeed);
            else if (relativeX > rotBufferdist)
                modelAnchor.localRotation = Quaternion.Slerp(modelAnchor.localRotation, Quaternion.Euler(0, transform.rotation.y, maxAngle), Time.deltaTime * rotSpeed);
            else
                modelAnchor.localRotation = Quaternion.Slerp(modelAnchor.localRotation, Quaternion.Euler(0, transform.rotation.y, 0), Time.deltaTime * rotSpeed);

            Vector3 destination = new Vector3(followPoint.position.x, transform.position.y, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * moveSpeed);          
        }
        
    }

    public void TriggerAnim(string anim)
    {
        //riderAnim.SetTrigger(anim);
    }
}
