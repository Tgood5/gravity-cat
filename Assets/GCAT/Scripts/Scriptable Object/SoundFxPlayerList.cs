﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gravity Cat/Sound Fx Player List")]
public class SoundFxPlayerList : ScriptableObject
{
    public AudioClip[] soundList;
    
    public AudioClip GetSound()
    {
        return soundList[Random.Range(0, soundList.Length)];
    }
}
