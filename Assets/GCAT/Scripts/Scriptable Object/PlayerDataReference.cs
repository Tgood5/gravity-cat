﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ONLY MAKE ONE OF THESE
[CreateAssetMenu(menuName = "Gravity Cat/Player Data Reference")]
public class PlayerDataReference : ScriptableObject
{
    public int coins;
    public int lives = 9;
    public Dictionary<Hat, bool> hatUnlocks;
    public Dictionary<Cat, bool> catUnlocks;

    public int equippedHatIndex = 0;
    public Cat equippedCat;

    public void LoadValuesFromData(PlayerData data)
    {
        coins = data.coins;
        lives = data.lives;

        for (int i = 0; i < AssetData.hatDataList.Count; i++)
            hatUnlocks.Add( AssetData.hatDataList[i], data.hatUnlocks[i]);
    }

    public PlayerData SetValuesToData(PlayerData data)
    {
        data.coins = coins;
        data.lives = lives;

        bool[] values = new bool[AssetData.hatDataList.Count];
        for (int i = 0; i < values.Length; i++)
        {
            bool value = true;
            hatUnlocks.TryGetValue(AssetData.hatDataList[i], out value);
            values[i] = value;
        }
        data.hatUnlocks = values;

        return data;
    }
}
