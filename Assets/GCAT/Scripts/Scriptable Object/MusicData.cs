﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gravity Cat/Music Data List")]
public class MusicData : ScriptableObject
{
    public MusicAudioClip[] soundList;

    [System.Serializable]
    public class MusicAudioClip
    {
        public Music music;
        public AudioClip clip;
    }

    public AudioClip GetAudioClip(Music music)
    {
        foreach (MusicAudioClip entry in soundList)
        {
            if (entry.music == music)
                return entry.clip;
        }
        Debug.Log("Error: Music [" + music + "] was not found!");
        return null;
    }
}
