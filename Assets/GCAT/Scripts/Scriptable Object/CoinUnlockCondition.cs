﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gravity Cat/Unlock Condition/Coin")]
public class CoinUnlockCondition : UnlockCondition
{
    public int cost;

    public override bool CheckCondition()
    {
        return AssetData.player.coins >= cost;
    }

    public override string GetDescription()
    {
        return string.Format("Spend {0} coins to unlock", cost);
    }

    public override void ApplyCost()
    {
        AssetData.player.coins -= cost;
    }
}
