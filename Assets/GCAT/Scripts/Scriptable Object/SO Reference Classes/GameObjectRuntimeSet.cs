using UnityEngine;

[CreateAssetMenu(menuName = "Gravity Cat/Game Object Runtime Set")]
public class GameObjectRuntimeSet : RuntimeSet<GameObject>{ }