﻿using UnityEngine;

public abstract class BaseVariable<T> : ScriptableObject
{
    public T value;
}
