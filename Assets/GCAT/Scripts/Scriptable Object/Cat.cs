﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gravity Cat/Cat")]
public class Cat : ScriptableObject
{
    public string displayName;
    public GameObject modelPrefab;
    public UnlockCondition condition;

    public bool isLocked()
    {
        if (condition == null)
            return true;
        else
            return condition.CheckCondition();
    }
}
