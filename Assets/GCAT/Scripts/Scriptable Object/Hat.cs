﻿using UnityEngine;

[CreateAssetMenu(menuName = "Gravity Cat/Hat")]
public class Hat : Unlockable
{
    public override void Unlock()
    {
        condition.ApplyCost();
        CollectionManager.UnlockHat(this);
    }

    public override string GetDescription()
    {
        if (condition != null)
            return condition.GetDescription();
        else
            return null;
    }
}
