﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class UnlockCondition : ScriptableObject
{
    public abstract bool CheckCondition();
    public abstract string GetDescription();
    public virtual void ApplyCost()
    {

    }
}
