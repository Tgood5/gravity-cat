﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gravity Cat/Hat Data")]
public class HatData : ScriptableObject
{
    public Hat[] hats;
}
