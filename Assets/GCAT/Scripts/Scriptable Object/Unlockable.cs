﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Unlockable : ScriptableObject
{
    public string displayName;
    public Sprite sprite;
    public UnlockCondition condition;

    public abstract string GetDescription();

    public abstract void Unlock();
}
