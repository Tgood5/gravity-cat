using System;

public abstract class BaseReference<T>
{
    public bool useConstant;
    public T constantValue;
    public BaseVariable<T> variable;

    public T value { 
        get { return useConstant ? constantValue : variable.value; }
    }
}
