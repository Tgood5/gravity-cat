﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gravity Cat/Sound Data List")]
public class SoundData : ScriptableObject
{
    public SoundAudioClip[] soundList;

    [System.Serializable]
    public class SoundAudioClip
    {
        public soundFx sound;
        public AudioClip clip;
    }

    public AudioClip GetAudioClip(soundFx sound)
    {
        foreach (SoundAudioClip entry in soundList)
        {
            if (entry.sound == sound)
                return entry.clip;
        }
        Debug.Log("Error: soundFX [" + sound + "] was not found!");
        return null;
    }
}
