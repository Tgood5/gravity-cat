using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDebug : MonoBehaviour
{
    public Text text;
    public StringBuilder sb;
    public Player player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        sb = new StringBuilder();
        sb.AppendLine("DEBUG:");
        sb.AppendLine("drag: "+player.body.drag);
        sb.AppendLine("y Velocity: " + player.body.velocity.y);
        sb.AppendLine("gravity scale: " + player.body.gravityScale);
        sb.AppendLine("grounded: " + player.detector.grounded);
        sb.AppendLine("jump Buffer: " + player.jumpBuffer);
        sb.AppendLine("coyote time: " + player.coyoteTime);

        text.text = sb.ToString();
    }
}
