using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class LevelSelectScreen : MonoBehaviour
{
    public const string selectLevelNotification = "LevelSlotSelected";

    public Selectable selected; // reference to most recently selected level
    public LevelSlot currentSlot;

    [SerializeField] Transform slotPanel = null;
    [SerializeField] GameObject slotPrefab = null;

    [SerializeField] TextMeshProUGUI displayNameText = null;
    [SerializeField] TextMeshProUGUI descriptionText = null;

    [SerializeField] TextMeshProUGUI coinsDisplay = null;
    [SerializeField] Image levelDisplay = null;

    [SerializeField] Button goButton = null;

    [SerializeField] MusicPlayList playList = null;

    #region monobehaviour
    private void Start()
    {      
        SetGUI();
        if (playList)
            SoundManager.StartNewPlaylist(playList);
    }
    private void OnEnable()
    {
        this.AddObserver(OnConfirm, selectLevelNotification);
    }
    private void OnDisable()
    {
        this.RemoveObserver(OnConfirm, selectLevelNotification);
    }
    #endregion
    void SetGUI()
    {
        //slotPanel.GetChild(0).GetComponent<Selectable>().Select();
        coinsDisplay.text = "x " + AssetData.player.coins;
        if(currentSlot != null)
        {
            displayNameText.text = currentSlot.name;
            levelDisplay.sprite = currentSlot.icon.sprite;
            descriptionText.text = currentSlot.description;
            goButton.gameObject.SetActive(!currentSlot.lockImage.enabled);
        }      
    }

    public void RenewSelection()
    {
        // TODO renew menu selection when using keyboard
        if (EventSystem.current.currentSelectedGameObject == null)
            selected.Select();
    }

    void GenerateSlots()
    {
        // TODO ASSETDATA LEVELDATA
        foreach (Hat hat in AssetData.hatDataList)
        {
            LevelSlot newSlot = Instantiate(slotPrefab, slotPanel).GetComponent<LevelSlot>();
            newSlot.icon.sprite = hat.sprite;
            newSlot.lockImage.enabled = false;
            newSlot.transform.position = Vector3.zero;
        }
    }

    public void BackButton()
    {
        TransitionManager.LoadScene(0); // dangerous hardcode, make scene enum/table?
    }

    void OnConfirm(object sender, object args)
    {
        if (sender is LevelSlot)
        {
            currentSlot = (LevelSlot)sender;
            SetGUI();
        }
    }

    public void GoButton()
    {
        if(currentSlot != null)
        {
            TransitionManager.LoadScene(currentSlot.levelSceneIndex);
        }
    }
}
