﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Selectable playButton;

    private void Start()
    {
        ScoreManager.Hide(false);
    }

    private void Update()
    {
        if (Input.GetAxisRaw("Vertical") != 0)
            RenewSelection();
    }

    // renew menu selection when using keyboard
    void RenewSelection()
    {
        if (EventSystem.current.currentSelectedGameObject == null)
            playButton.Select();
    }

    #region Buttons
    public void PlayButton()
    {
        TransitionManager.LoadScene(4);
    }

    public void CollectionButton()
    {
        TransitionManager.LoadScene(3);
    }

    public void SettingsButton()
    {
        // TODO
    }

    public void QuitButton()
    {
        Application.Quit();
    }
    #endregion
}
