﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    // inspector
    [SerializeField] GameObject gameOverText = null;
    [SerializeField] TextMeshPro finalScoreText = null;
    [SerializeField] Selectable returnToMenuButton = null;
    [SerializeField] Selectable retryButton = null;
    [SerializeField] Transform buttonAnchor = null;
    [SerializeField] EvilCat evil = null;
    [SerializeField] Player player = null;
    [SerializeField] Transform saucerP0 = null;
    [SerializeField] Transform saucerP1 = null;
    [SerializeField] Transform saucerP2 = null;

    private void Start()
    {
        iTween.ScaleTo(gameOverText, iTween.Hash("x", 1.3, "y", 1.3,"looptype","pingPong","time", 1));
        SoundManager.PlayMusic(Music.Untimely_Defeat);
        if(ScoreManager.GetRemainingLives() > 0)
            retryButton.gameObject.SetActive(true);
        else
            retryButton.gameObject.SetActive(false);

        evil.TriggerCatAnim(EvilCat.LAUGH_ANIM);
        evil.SetFollowTransform(saucerP1);
        evil.SetFollowing(true);
        evil.SetFocusTransform(player.transform);
        // player defeat anim
        player.animator.SetBool("KO", true);
    }

    private void Update()
    {
        if (Input.GetAxisRaw("Horizontal") != 0)
            RenewSelection();
    }

    // renew menu selection when using keyboard
    void RenewSelection()
    {
        if (EventSystem.current.currentSelectedGameObject == null)
            retryButton.Select();
    }

    // button functions
    public void RetryButton()
    {
        buttonAnchor.gameObject.SetActive(false);
        StartCoroutine(ContinueAnim());      
    }

    public void QuitButton()
    {
        buttonAnchor.gameObject.SetActive(false);
        StartCoroutine(QuitAnim());
    }

    public void MenuReturnButton()
    {
        returnToMenuButton.gameObject.SetActive(false);
        TransitionManager.LoadScene(0);
    }

    IEnumerator ContinueAnim()
    {
        // remove life, reset multiplier, apply score penalty
        ScoreManager.SetMultiplier(1);
        ScoreManager.MultiplyPoints(0.1f);
        ScoreManager.AwardLives(-1);
        yield return null;

        gameOverText.gameObject.SetActive(false);
        evil.TriggerCatAnim(EvilCat.ANGRY_ANIM);
        player.animator.SetBool("KO", false);
        player.Jump();
        player.SetSprintState(true);

        // wait for score anim
        while (ScoreManager.IsAnimating())
            yield return null;

        // wait for cat animation
        player.SetDirectionManual(Vector2.right);
        evil.SetFollowTransform(saucerP2);
        while (!evil.IsNearFollowPoint())
            yield return null;

        yield return new WaitForSeconds(1f);
        TransitionManager.LoadScene(TransitionManager.GetPreviousSceneIndex());
    }

    IEnumerator QuitAnim()
    {
        finalScoreText.text = "" + ScoreManager.totalScore;
        gameOverText.gameObject.SetActive(false);
        returnToMenuButton.gameObject.SetActive(true);

        // reset points, multiplier, lives
        ScoreManager.SetMultiplier(1);
        ScoreManager.SetPoints(0);
        ScoreManager.SetLives(9);
        yield return null;

        // evil cat animation
        evil.SetFollowTransform(saucerP0);
        //while (!evil.IsNearFollowPoint())
        //    yield return null;

        // animate final score text
        for (int i = 0; i<8; i++)
        {
            finalScoreText.gameObject.SetActive(!finalScoreText.gameObject.activeSelf);
            yield return new WaitForSecondsRealtime(0.8f);
        }
        finalScoreText.gameObject.SetActive(true);

        // wait for score animation
        //while (ScoreManager.IsAnimating())
        //    yield return null;
       
        //yield return new WaitForSeconds(1f);      
    }
}
