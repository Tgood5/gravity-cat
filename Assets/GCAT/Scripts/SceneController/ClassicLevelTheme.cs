﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassicLevelTheme : LevelTheme
{
    public bool sinkBaseOnStart = false;
    public float baseBlockSpeed;
    public float baseSpawnTimer;
    public BlockSpawner platSpawns;
    public ParticleSystem bgSpawns;
    public Color[] bgColors;
    int colorIndex = 0;

    // todo parallax bg??
    public Sprite bgSprite;

    public override void Activate(object args)
    {
        ClassicLevelController controller = args as ClassicLevelController;
        controller.baseBlockSpeed = baseBlockSpeed;
        controller.baseSpawnTimer = baseSpawnTimer;
        platSpawns.spawning = true;
        bgSpawns.Play();
        PickRandomColor();
        StartCoroutine(ColorLerp());
        //if (sinkBaseOnStart)
            //controller.SinkBase(5);
    }

    public override void Deactivate()
    {
        platSpawns.spawning = false;
        bgSpawns.Stop();
    }

    void PickRandomColor()
    {
        if (bgColors.Length <= 1)
            return;

        int newIndex = colorIndex;
        while (newIndex == colorIndex)
            newIndex = Random.Range(0, bgColors.Length);
        colorIndex = newIndex;
    }

    IEnumerator ColorLerp()
    {
        float duration = 2f;
        float elapsed = 0;
        Camera mainCam = Camera.main;
        Color initCol = mainCam.backgroundColor;
        while (elapsed < duration)
        {
            mainCam.backgroundColor = Color.Lerp(initCol, bgColors[colorIndex], elapsed / duration);
            elapsed += Time.unscaledDeltaTime;
            yield return null;
        }
    }
}
