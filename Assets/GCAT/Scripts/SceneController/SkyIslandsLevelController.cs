using Cinemachine;
using ScriptableObjectArchitecture;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.PlayerSettings;

public class SkyIslandsLevelController : MonoBehaviour
{
    public const float DEFAULT_PLAT_HEIGHT_DIFF = 4.0f;

    // inspector   
    [SerializeField] Transform cameraHeightThreshold; // camera will move up if cat lands above this position.y
    [SerializeField] BoxCollider2D playArea;
    [SerializeField] CollapserSpawner spawner;
    [SerializeField] Transform spawnerAnchor;
    [SerializeField] MusicPlayList playList;

    public FloatReference collapseTimer;
    public float initFallSpeed = 1;
    public FloatReference fallSpeed;
    public float initSpawnTime = 2; // (not for plats)
    public FloatReference spawnTimer;

    [SerializeField] float distanceTraveled = 0.0f; // track vertical units traveled. used to spawn platforms

    [SerializeField] float spawnInterval = 0.1f;
    [SerializeField] float spawnTime = 0.0f;

    public float ascendSpeed = 0.1f; // rate at which camera will move up automatically

    private void Start()
    {
        // init stuff
        spawner.collapseTimeRef = collapseTimer;
        spawner.speedRef = fallSpeed;
        spawner.timerRef = spawnTimer;
        fallSpeed.Value = initFallSpeed;
        spawnTimer.Value = initSpawnTime;
        ScoreManager.SetTimer(0, 0, false);
        ScoreManager.StartTimer();
        SoundManager.StartNewPlaylist(playList);

        // start level
        distanceTraveled += playArea.size.y;      
    }
    private void OnEnable()
    {
        this.AddObserver(OnGameLose, Player.gameOverNotification);
        this.AddObserver(OnGameWin, WinLoseCondition.OnConditionWin);
        this.AddObserver(OnBlockCollapse, CollapsingBlock.BlockCollapseNotification);
    }
    private void OnDisable()
    {
        this.RemoveObserver(OnGameLose, Player.gameOverNotification);
        this.RemoveObserver(OnGameWin, WinLoseCondition.OnConditionWin);
        this.RemoveObserver(OnBlockCollapse, CollapsingBlock.BlockCollapseNotification);
    }
    private void Update()
    {
        distanceTraveled += ascendSpeed * Time.deltaTime;
        playArea.transform.Translate(Vector3.up * ascendSpeed * Time.deltaTime);

        spawnTime += Time.deltaTime;
        if(spawnTime > spawnInterval)
        {
            if(distanceTraveled > DEFAULT_PLAT_HEIGHT_DIFF)
            {
                spawnTime = 0.0f;
                distanceTraveled -= DEFAULT_PLAT_HEIGHT_DIFF;
                SpawnPlats();
            }
        }
    }

    public void SpawnPlats(bool ignoreHeight = false)
    {   
        spawner.SpawnRandom();       
        if (!ignoreHeight)
            spawnerAnchor.transform.Translate(new Vector2(0, DEFAULT_PLAT_HEIGHT_DIFF));
    }

    private void OnBlockCollapse(object sender, object args)
    {
        var plat = sender as CollapsingBlock; 
        if(plat != null)
        {
            if (plat.transform.position.y > cameraHeightThreshold.position.y)
                StartCoroutine(MoveCameraUp((plat.transform.position.y - cameraHeightThreshold.position.y) + (playArea.size.y/2)));
        }       
    }
    private IEnumerator MoveCameraUp(float dist = 4)
    {
        Vector3 startpos = playArea.transform.position;
        Vector3 endpos = playArea.transform.position;
        endpos.y += dist;

        distanceTraveled += dist;
        float duration = 1f; // todo ??
        float elapsed = 0f;

        SoundManager.PlaySound(soundFx.rumble);
        while (elapsed <= duration)
        {
            elapsed += Time.deltaTime;
            playArea.transform.position = Vector3.Lerp(startpos, endpos, elapsed / duration);
            yield return null;
        }
        playArea.transform.position = endpos;
        yield return null;
        
    }

    void OnGameLose(object sender, object args)
    {
        //GameOver = true;
        StopAllCoroutines();
        ScoreManager.StopTimer();
        //this.PostNotification(GameOverNotification);
        StartCoroutine(AnimateGameLose());
    }
    void OnGameWin(object sender, object args)
    {
        //GameOver = true;
        StopAllCoroutines();
        //this.PostNotification(GameOverNotification);
        StartCoroutine(AnimateGameWin());
    }
    IEnumerator AnimateGameWin()
    {
        // TODO
        yield return null;
        Debug.Log("LEVEL WIN!!!");
    }
    IEnumerator AnimateGameLose()
    {
        yield return new WaitForSecondsRealtime(2f);
        TransitionManager.LoadScene(2);
    }
}
