﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class CollectionScreen : MonoBehaviour
{
    public const string selectItemNotification = "collectionScreen.SelectItem";

    public Animator modelAnim;
    public Selectable selected; // reference to most recently selected selectable
    public CollectionSlot currentSlot;
  
    [SerializeField] Transform slotPanel = null;
    [SerializeField] GameObject slotPrefab = null;

    [SerializeField] TextMeshProUGUI displayNameText = null;
    [SerializeField] TextMeshProUGUI descriptionText = null;

    [SerializeField] TextMeshProUGUI coinsDisplay = null;
    [SerializeField] SpriteRenderer hatDisplay = null;

    [SerializeField] Button unlockButton = null;

    [SerializeField] MusicPlayList playList = null;

    private void Start()
    {
        modelAnim.SetBool("grounded", true);
        SoundManager.StartNewPlaylist(playList);

        GenerateSlots();
        SetGUI();
    }

    private void Update()
    {
        coinsDisplay.text = "x " + AssetData.player.coins;
        if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            RenewSelection();
    }

    private void OnEnable()
    {
        this.AddObserver(OnConfirm, selectItemNotification);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnConfirm, selectItemNotification);
    }

    void SetGUI()
    {
        slotPanel.GetChild(0).GetComponent<Selectable>().Select();
        coinsDisplay.text = "x "+ AssetData.player.coins;
        Hat currentHat = CollectionManager.GetEquippedHat();
        hatDisplay.sprite = currentHat.sprite;
        descriptionText.text = currentHat.GetDescription();
    }

    // renew menu selection when using keyboard
    void RenewSelection()
    {
        if (EventSystem.current.currentSelectedGameObject == null)
            selected.Select();
    }

    void GenerateSlots()
    {
        foreach(Hat hat in AssetData.hatDataList)
        {
            CollectionSlot newSlot = Instantiate(slotPrefab, slotPanel).GetComponent<CollectionSlot>();
            newSlot.icon.sprite = hat.sprite;
            if (CollectionManager.CheckHatUnlocked(hat))
                newSlot.lockImage.enabled = false;
            else
                newSlot.lockImage.enabled = true;
            newSlot.unlockable = hat;
            newSlot.collection = this;

            newSlot.transform.position = Vector3.zero;
        }
    }

    public void SelectUI(Selectable toSelect)
    {
        selected = toSelect;
        if (selected is CollectionSlot)
            SelectSlot((CollectionSlot)selected);
    }

    public void BackButton()
    {
        TransitionManager.LoadScene(TransitionManager.GetPreviousSceneIndex());
    }

    // update text ui, show unlock button if locked
    void SelectSlot(CollectionSlot slot)
    {
        displayNameText.text = slot.unlockable.displayName;
        descriptionText.text = slot.unlockable.GetDescription();
        currentSlot = slot;
        if (slot.lockImage.enabled)
        {
            unlockButton.gameObject.SetActive(true);
            if (slot.unlockable.condition.CheckCondition() == false)
                unlockButton.interactable = false;
        }
        else
        {
            unlockButton.gameObject.SetActive(false);
            unlockButton.interactable = true;
        }
            
    }

    // equip hat if unlocked
    void OnConfirm(object sender, object args)
    {
        if(args is Hat)
        {
            Hat hat = args as Hat;
            hatDisplay.sprite = hat.sprite;
            if (CollectionManager.CheckHatUnlocked(hat))
            {
                CollectionManager.EquipHat(hat);            
            }         
        }
    }

    // unlock hat
    public void UnlockButton()
    {
        currentSlot.unlockable.Unlock();
        currentSlot.lockImage.enabled = false;
        unlockButton.gameObject.SetActive(false);
        SoundManager.PlaySound(soundFx.purchase);
        Hat hat = currentSlot.unlockable as Hat;
        if(hat != null)
            CollectionManager.EquipHat(hat);
    }


}
