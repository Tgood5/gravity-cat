﻿using System.Collections;
using ScriptableObjectArchitecture;
using UnityEngine;
using Cinemachine;

public class ClassicLevelController : MonoBehaviour
{
    public const string GameOverNotification = "GameOverNotification";

    public bool levelIsActivated = false;
    
    //inspector
    public Transform blockHeightThreshold;   
    public Transform baseLevel;
    public Lava lava;
    public ParticleSystem lavaSmoke;
    public ParticleSystem speedFx;
    public FloatReference blockSpeed;
    
    public FloatReference spawnTimer;
    [SerializeReference] public LevelTheme[] levelThemes;
    [SerializeField] MusicPlayList musicPlayList;
    public int levelIndex = 0;
    public GameObjectCollection allBlocks;

    public float baseBlockSpeed = 1f;
    public FloatReference beginningBlockSpeed;
    public FloatReference beginningSpawnTimer;
    public FloatReference beginningNormalSpeedOffset;
    public FloatReference beginningNormalSpeedDuration;
    public float blockSpeedMult = 1f;
    public float baseSpawnTimer = 5f;
    public float spawnTimerMult = 1f;
    public float levelRate = 0.2f;
    public float levelTimer = 60f;
    public float levelTime = 0f;
    public float speedTimer = 20f;
    public float speedTime = 0f;
    public float level = 1;

    float scoreTimer = 1f;
    float scoreTime = 0f;

    bool GameOver = false;

    private void Start()
    {
        var emission = lavaSmoke.emission;
        emission.rateOverTime = 0;
        blockSpeed.Value = baseBlockSpeed;
        spawnTimer.Value = baseSpawnTimer;
        StartLevel();
    }

    private void OnEnable()
    {
        this.AddObserver(OnGameLose, Player.gameOverNotification);
        this.AddObserver(OnGameWin, WinLoseCondition.OnConditionWin);
        this.AddObserver(OnBlockLanded, StackerBlock.blockLandingNotification);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameLose, Player.gameOverNotification);
        this.RemoveObserver(OnGameWin, WinLoseCondition.OnConditionWin);
        this.RemoveObserver(OnBlockLanded, StackerBlock.blockLandingNotification);
    }

    private void Update()
    {
        if (!GameOver)
        {
            levelTime += Time.deltaTime;
            scoreTime += Time.deltaTime;
            speedTime += Time.deltaTime;

            // increase score
            if (scoreTime > scoreTimer)
            {
                scoreTime = 0f;
                ScoreManager.AwardPoints(1);
            }

            //increase speed
            if(speedTime > speedTimer)
            {
                speedTime = 0f;
                speedFx.Play();
                blockSpeedMult *= 1 + levelRate;
                spawnTimerMult *= 0.8f;
                ScoreManager.AwardMultiplier(1);
            }

            // increase level
            if (levelTime > levelTimer)
            {
                levelTime = 0f;
                level++;
                ScoreManager.AwardMultiplier(5);              
                levelThemes[levelIndex].Deactivate();
                levelIndex++;
                if (levelIndex >= levelThemes.Length)
                    levelIndex = 0;
                StartCoroutine(AnimateNextLevel(levelThemes[levelIndex]));
            }
        }     
    }

    // start gameplay
    void StartLevel()
    {
        // score
        ScoreManager.SetTimer(0, 0, false);
        ScoreManager.StartTimer();
        ScoreManager.Show();

        // sound
        musicPlayList.Shuffle();
        SoundManager.StartNewPlaylist(musicPlayList);
        //SoundManager.PlayMusic(Music.Showdown);
        
        levelIsActivated = true;
        levelThemes[levelIndex].Activate(this);
        StartCoroutine(SpeedUpBlockDecentTillBlockReachesElevation());
    }
    private IEnumerator SpeedUpBlockDecentTillBlockReachesElevation()
    {
        blockSpeed.Value = beginningBlockSpeed.Value;
        spawnTimer.Value = beginningSpawnTimer.Value;
        var blocks = allBlocks;

        //go fast till the first block gets close to the block height threshold line.
        while (blocks.Count < 1 || blocks[0].transform.position.y > blockHeightThreshold.position.y + beginningNormalSpeedOffset.Value) 
        { 
            yield return null;
        }

        var current = 0f;
        var spawnerCurrent = 0f;

        //Gradually slow down to the normal play speed over one second. Yes ideally some of these Values shouldn't be hard coded but thats the way it is for now.
        while (spawnerCurrent <= beginningNormalSpeedDuration.Value)
        {
            current += Time.deltaTime;
            //3.5 seemed to be the magic number for getting the blocks to spawn at the correct time during the transition.
            //This could be avoided by either having the spawn time and the speed be linked so if speed changes the spawn time will automatically as well.
            //Another solution could be to have a Threshold similar to the blockHeightThreshold that will cause the blocks to spawn.
            spawnerCurrent += Time.deltaTime/3.5f; 
                                                    
            blockSpeed.Value = Mathf.Lerp(beginningBlockSpeed.Value, baseBlockSpeed, current / beginningNormalSpeedDuration.Value);
            spawnTimer.Value = Mathf.Lerp(beginningSpawnTimer.Value, baseSpawnTimer, spawnerCurrent / beginningNormalSpeedDuration.Value);
            yield return null;
        }

        //spawnTimer.Value = baseSpawnTimer;
    }

    public void EndLevel()
    {
        // todo
        levelIsActivated = false;
    }

    void OnBlockLanded(object sender, object args)
    {
        var pos = (Vector3)args;
        (sender as Transform).SetParent(baseLevel);

        if (!GameOver)
        {
            SoundManager.PlaySound(soundFx.impact);
            //Debug.Log(string.Format("block landed at pos: {0} current blockHeightThreshold = {1}", pos.y, blockHeightThreshold.position.y));
            if (pos.y > blockHeightThreshold.position.y)
            {
                StartCoroutine(AnimateSinkBase(pos));
            }
        }       
    }

    public void SinkBase(int units)
    {
        StartCoroutine(AnimateSinkBase(blockHeightThreshold.position, units));
    }

    private IEnumerator AnimateSinkBase(Vector3 pos, float units = 0f)
    {
        //TODO shake screen
        CinemachineBasicMultiChannelPerlin shaker = CameraShaker.shaker;
        shaker.m_AmplitudeGain = 1f;

        units = units == 0 ? pos.y - blockHeightThreshold.position.y : units;

        while (units > 0)
        {
            units--;

            Vector3 startpos = baseLevel.transform.position;
            Vector3 endpos = startpos + Vector3.down;
            float duration = 1f / blockSpeed.Value;
            float current = 0f;
            var emission = lavaSmoke.emission;
            emission.rateOverTime = 40;

            lava.Splash(0, 2);
            SoundManager.PlaySound(soundFx.rumble);
            while (current <= duration)
            {
                current += Time.deltaTime;
                baseLevel.position = Vector3.Lerp(startpos, endpos, current / duration);
                yield return null;
            }
            baseLevel.position = endpos;
            emission.rateOverTime = 0;
            yield return null;
        }

        shaker.m_AmplitudeGain = 0;        
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(blockHeightThreshold.position, blockHeightThreshold.position + Vector3.right * 5f);
    }

    void OnGameLose(object sender, object args)
    {
        GameOver = true;
        StopAllCoroutines();
        ScoreManager.StopTimer();
        this.PostNotification(GameOverNotification);
        StartCoroutine(AnimateGameOver());
    }

    void OnGameWin(object sender, object args)
    {
        GameOver = true;
        StopAllCoroutines();
        this.PostNotification(GameOverNotification);
        StartCoroutine(AnimateGameWin());
    }

    IEnumerator AnimateGameOver()
    {
        float endPos = lava.transform.position.y + ((Screen.height/32) * 0.55f);

        // fill screen with lava
        while(lava.transform.position.y < endPos)
        {
            lava.transform.Translate(Vector3.up * Time.deltaTime * 10);
            yield return null;
        }
        yield return new WaitForSecondsRealtime(0.2f);
        TransitionManager.LoadScene(2);
    }

    IEnumerator AnimateNextLevel(LevelTheme theme)
    {
        //Time.timeScale = 0;
        //ScoreManager.StopTimer();
        //yield return new WaitForSecondsRealtime(1.5f);

        theme.Activate(this);
        UpdateBlockSpeed();
        //todo animate show next level text

        yield return null;

        //Time.timeScale = 1;
        //ScoreManager.StartTimer();
    }

    // BIG TODO:
    IEnumerator AnimateGameWin()
    {
        //spawner.spawning = false;
        //ScoreManager.StopTimer();
        yield return null;

        Debug.Log("LEVEL WIN!!!");
    }

    void UpdateBlockSpeed()
    {
        blockSpeed.Value = baseBlockSpeed * blockSpeedMult;
        spawnTimer.Value = baseSpawnTimer * spawnTimerMult;
    }
}
