﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public abstract class LevelTheme : MonoBehaviour
{
    public abstract void Activate(object args);
    public abstract void Deactivate();
}
