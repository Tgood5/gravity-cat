using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Used to expose functions for Animation Events for Evil Cat
public class EvilCatAnimEventCaller : MonoBehaviour
{
    // set in inspector
    [SerializeField] EvilCat evil;

    public void ToggleBeam(bool toggleOn)
    {
        evil.ToggleBeam(toggleOn);
    }

    public void ToggleBeamOn()
    {
        ToggleBeam(true);
    }
    public void ToggleBeamOff()
    {
        ToggleBeam(false);
    }

    private void Start()
    {
        evil.ToggleBeam(false);
    }
}
