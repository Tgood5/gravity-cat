﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    // access publicly via helper methods
    protected static T instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = GetComponent<T>();
            DontDestroyOnLoad(gameObject);
            InitSingletonInstance();
        }        
        else
        {
            Destroy(gameObject);
        }
    }

    // use to initialize the first created instance
    protected abstract void InitSingletonInstance();
    
}
