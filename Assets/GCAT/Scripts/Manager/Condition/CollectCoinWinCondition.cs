﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectCoinWinCondition : WinLoseCondition
{
    public int coinsToWin = 10;
    void Start()
    {
        this.AddObserver(OnCoinCollected, CatCoin.CoinCollectNotification);
    }
    private void OnDestroy()
    {
        this.RemoveObserver(OnCoinCollected, CatCoin.CoinCollectNotification);
    }
    void OnCoinCollected(object sender, object args)
    {
        CheckWin();
    }

    public override bool CheckWin()
    {
        if (AssetData.player.coins >= coinsToWin)
        {
            this.RemoveObserver(OnCoinCollected, CatCoin.CoinCollectNotification);
            this.PostNotification(OnConditionWin);
            return true;
        }
            
        return false;
    }
}
