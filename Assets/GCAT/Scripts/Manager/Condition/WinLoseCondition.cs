﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WinLoseCondition : MonoBehaviour
{
    public const string OnConditionWin = "Condition_Win_Notification";
    public virtual bool CheckWin()
    {
        return false;
    }
    public virtual bool CheckLose()
    {
        return false;
    }
}
