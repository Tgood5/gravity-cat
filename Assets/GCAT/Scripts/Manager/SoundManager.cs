﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;

public class SoundManager : Singleton<SoundManager>
{
    [SerializeField] CanvasGroup canvasGroup;
    [SerializeField] Button musicToggleButton;
    [SerializeField] TextMeshProUGUI musicToggleText;
    [SerializeField] Button sfxToggleButton;
    [SerializeField] TextMeshProUGUI sfxToggleText;
    [SerializeField] Transform sfxAnchor;
    const string ON_TEXT = "On";
    const string OFF_TEXT = "Off";
    public static bool musicOn = true;
    public static bool sfxOn = true;

    public SoundData soundData;
    public MusicData musicData;

    public AudioSource musicSource;
    public AudioSource ambienceSource;
    public AudioMixerGroup sfxMixer;

    public MusicPlayList musicList; // music available to play

    private Dictionary<soundFx, AudioSource> prefabList;
    void Update()
    {
        if (!musicSource.isPlaying)
        {
            if (musicList != null)
                InstancePlayMusic(musicList.GetNext());
        }    
    }

    protected override void InitSingletonInstance()
    {
        prefabList = new Dictionary<soundFx, AudioSource>();
    }

    public static void PlaySound(soundFx sound)
    {
        if (instance != null)
            instance.InstancePlaySoundFX(sound);
        else
            Debug.Log("no soundmanager instance!");
    }

    void InstancePlaySoundFX(soundFx sound)
    {
        
        AudioSource source = null; 
        if (!prefabList.TryGetValue(sound, out source))
        {
            GameObject obj = new GameObject("sound");
            source = obj.AddComponent<AudioSource>();
            source.clip = instance.soundData.GetAudioClip(sound);
            source.outputAudioMixerGroup = sfxMixer;
            source.playOnAwake = false;
            prefabList.Add(sound, source);
            obj.transform.SetParent(sfxAnchor, true);
        }

        if (sfxAnchor.gameObject.activeSelf)
            source.Play();
    }

    public static void PlayMusic(Music music)
    {
        if (instance != null)
            instance.InstancePlayMusic(music);
        else
            Debug.Log("no soundmanager instance!");
    }

    void InstancePlayMusic(AudioClip newClip)
    {
        musicSource.Pause();
        musicSource.clip = newClip;
        musicSource.Play();
    }
    void InstancePlayMusic(Music music)
    {
        InstancePlayMusic(musicData.GetAudioClip(music));
    }

    public static void StartNewPlaylist(MusicPlayList pList)
    {
        instance.musicList = pList;
        instance.InstancePlayMusic(pList.GetCurrentClip());
    }
    public static void ShufflePlaylist()
    {
        instance.musicList.Shuffle();
        instance.InstancePlayMusic(instance.musicList.GetCurrentClip());
    }
    public static void PlayNextMusic()
    {
        instance.InstancePlayMusic(instance.musicList.GetNext());
    }
    public static void ToggleMusic()
    {
        musicOn = !musicOn;
        instance.musicSource.mute = !musicOn;
        if (musicOn)
            instance.musicToggleText.text = ON_TEXT;
        else
            instance.musicToggleText.text = OFF_TEXT;
    }

    public static void ToggleSFX()
    {
        sfxOn = !sfxOn;
        instance.sfxAnchor.gameObject.SetActive(sfxOn);
        if (sfxOn)
            instance.sfxToggleText.text = ON_TEXT;
        else
            instance.sfxToggleText.text = OFF_TEXT;
    }
}
