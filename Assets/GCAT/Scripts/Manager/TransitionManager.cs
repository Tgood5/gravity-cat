﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class TransitionManager : Singleton<TransitionManager>
{
    [SerializeField] Animator animator = null;
    [SerializeField] Canvas canvas = null;

    int currentSceneIndex;
    int previousSceneIndex;
    Coroutine transition = null;

    protected override void InitSingletonInstance()
    {       
        canvas.gameObject.SetActive(true);
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    private void Start()
    {
        animator.SetBool("Hide", false);
    }

    public static void LoadScene(int index)
    {
        if (instance.transition == null)
            instance.StartCoroutine(instance.Transition(index));
    }

    public static int GetPreviousSceneIndex()
    {
        return instance.previousSceneIndex;
    }

    IEnumerator Transition(int index)
    {
        Time.timeScale = 0f;
        animator.SetBool("Hide", true);
        yield return new WaitForSecondsRealtime(0.6f);

        SceneManager.LoadScene(index);
        previousSceneIndex = currentSceneIndex;
        currentSceneIndex = index;

        animator.SetBool("Hide", false);
        yield return new WaitForSecondsRealtime(0.6f);
        Time.timeScale = 1f;
    }
}
