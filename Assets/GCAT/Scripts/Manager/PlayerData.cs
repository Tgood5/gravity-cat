﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public string fileName;
    public int lives;
    public int coins;

    public bool[] hatUnlocks;
    public bool[] catUnlocks;
}
