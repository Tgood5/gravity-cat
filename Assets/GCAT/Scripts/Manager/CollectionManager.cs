﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionManager : Singleton<CollectionManager>
{
    // singleton
    protected override void InitSingletonInstance()
    {
        
    }

    public static bool CheckHatUnlocked(Hat hat)
    {
        if (hat.condition == null)
            return true;
        if(AssetData.player.hatUnlocks != null)
            return AssetData.player.hatUnlocks[hat];
        return false;
    }

    public static void UnlockHat(Hat hat)
    {
        AssetData.player.hatUnlocks[hat] = true;
    }

    public static Hat GetEquippedHat()
    {
        return AssetData.hatDataList[AssetData.player.equippedHatIndex];
    }

    public static Sprite GetHatSprite()
    {
        if (instance != null)
            return GetEquippedHat().sprite;
        return null;
    }

    public static void EquipHat(Hat hat)
    {
        if (hat == null)
            AssetData.player.equippedHatIndex = 0;
        else
        {
            for(int i=0; i< AssetData.hatDataList.Count; i++)
            {
                if(AssetData.hatDataList[i] == hat)
                {
                    AssetData.player.equippedHatIndex = i;
                    return;
                }
            }
        }
    }
}
