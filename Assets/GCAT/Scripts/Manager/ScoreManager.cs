﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : Singleton<ScoreManager>
{
    // score
    public static int totalScore { get; private set; }
    public int multiplier { get; private set; }
    public int displayScore { get; private set; }   // for animation
    public int displayMult { get; private set; }

    int activeRoutines = 0;

    // lives
    const int MAX_LIVES = 9;
    public int lives = MAX_LIVES;

    // timer
    public TimeCounter timer;

    bool isAnimating;

    [Header("Inspector")]
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI multiText;
    public TextMeshProUGUI livesText;
    public TextMeshProUGUI coinsText;
    public Image livesImage;
    public Image coinsImage;
    public Canvas canvas;

    protected override void InitSingletonInstance()
    {
        ResetPoints();
    }

    private void Start()
    {
        // todo: make this dependent on level scenario??
        this.AddObserver(OnCoinCollectDefault, CatCoin.CoinCollectNotification);
    }

    private void Update()
    {
        if(displayScore != totalScore || displayMult != multiplier)
        {
            isAnimating = true;
            int increment = 1;
            int difference = Mathf.Abs(totalScore - displayScore);
            if (difference > 500)
                increment = (int)(difference * 0.02f);
            else if (difference > 50)
                increment = 10;
            
            if (displayScore > totalScore)
                displayScore -= increment;
            else if (displayScore < totalScore)
                displayScore += increment;

            scoreText.SetText("Score: " + displayScore);

            if (displayMult > multiplier)
                displayMult--;
            else if(displayMult < multiplier)
                displayMult++;

            multiText.SetText("x" + displayMult);
        }
        else
        {
            isAnimating = false;
        }
        
    }

    #region Public
    public static void Show(bool fade = true)
    {
        instance.canvas.enabled = true;
        UpdateUI();
    }

    public static void Hide(bool fade = true)
    {
        if(instance!=null)
            instance.canvas.enabled = false;
    }

    public static void SetPoints(int value)
    {
        totalScore = value;
    }
    public static void MultiplyPoints(float multiplier)
    {
        // multiply total score
        totalScore = (int)(totalScore * multiplier);
    }

    public static void SetMultiplier(int value)
    {
        instance.multiplier = value;
    }
    public static void SetLives(int value)
    {
        instance.lives = value;
    }
    public static void ResetPoints()
    {
        SetPoints(0);
        SetMultiplier(1);       
    }

    public static void AwardPoints(int value)
    {
        totalScore += (int)(value * instance.multiplier);
        //instance.AnimateScore();
    }

    public static void AwardMultiplier(int value)
    {
        instance.multiplier += value;
        instance.AnimateMultiplier();
    }

    public static void AwardLives(int value)
    {
        instance.lives += value;
        Mathf.Clamp(instance.lives, 0, MAX_LIVES);
        instance.livesText.SetText("x" + instance.lives);
        instance.AnimateLives();
    }
    public static void AwardCoins(int value)
    {
        AssetData.player.coins += value;
        instance.coinsText.text = AssetData.player.coins.ToString();
        instance.AnimateCoins();
    }

    public static void UpdateUI()
    {
        instance.livesText.SetText("x" + instance.lives);
        instance.coinsText.text = AssetData.player.coins.ToString();
    }

    public static int GetRemainingLives()
    {
        return instance.lives;
    }

    public static void StopTimer()
    {
        instance.timer.enabled = false;
    }

    public static void StartTimer()
    {
        instance.timer.enabled = true;
    }

    public static void SetTimer(int min, int sec, bool countdown)
    {
        instance.timer.minutes = min;
        instance.timer.seconds = sec;
        instance.timer.totalTime = (min * 60) + sec;
        instance.timer.countDown = countdown;
        instance.timer.SetDisplay();
    }

    
    public static bool IsAnimating()
    {
        return instance.isAnimating || instance.activeRoutines > 0;
    }

    public static PlayerData SetValuesToData(PlayerData data)
    {
        if(instance != null)
            data.lives = instance.lives;
        return data;
    }

    public static void LoadValuesFromData(PlayerData data)
    {
        instance.lives = data.lives;
    }
    #endregion

    public void OnCoinCollectDefault(object sender, object args)
    {
        AwardMultiplier(1);
        AwardPoints(10);
        AwardCoins(1);
    }

    #region animation
    void AnimateMultiplier()
    {
        StartCoroutine(ScaleBump(multiText.gameObject, 1.3f, 0.5f));
    }
    void AnimateCoins()
    {
        StartCoroutine(ScaleBump(coinsText.gameObject, 1.3f, 0.5f));
    }
    void AnimateScore()
    {
        StartCoroutine(ScaleBump(scoreText.gameObject, 1.3f, 0.5f));
    }
    void AnimateLives()
    {
        StartCoroutine(BlinkObject(instance.livesImage.gameObject, 5));
    }

    IEnumerator ScaleBump(GameObject obj, float scale, float duration)
    {
        instance.activeRoutines++;
        obj.transform.localScale = Vector3.one;
        
        iTween.ScaleTo(obj, iTween.Hash("time", duration/2f, "x", scale, "y", scale, "looptype", "pingPong"));
        yield return null;
        float elapsed = 0f;
        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            yield return null;
        }

        iTween tweener = obj.GetComponent<iTween>();
        if (tweener)
            Destroy(tweener);

        obj.transform.localScale = Vector3.one;
        instance.activeRoutines--;
    }

    IEnumerator BlinkObject(GameObject obj, int blinks)
    {
        activeRoutines++;
        int count = 0;
        while(count < blinks * 2)
        {
            count++;
            obj.SetActive(!obj.activeSelf);
            yield return new WaitForSeconds(0.15f);
        }

        obj.SetActive(true);
        activeRoutines--;
    }
    #endregion
}
