﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// holds SO reference to global vars
public class AssetData : Singleton<AssetData>
{
    [SerializeField] HatCollection _hatDataList = null;
    [SerializeField] PlayerDataReference _playerData = null;

    protected override void InitSingletonInstance()
    {
        if (_hatDataList == null)
            Debug.LogError("No HatData reference!");
        if (_playerData == null)
            Debug.LogError("No PlayerDataReference!");

        player.coins = 0;
        player.lives = 9;
        player.hatUnlocks = new Dictionary<Hat, bool>();
        player.catUnlocks = new Dictionary<Cat, bool>();
        player.equippedHatIndex = 0;
    }

    public static HatCollection hatDataList { get { return instance._hatDataList; } }
    public static PlayerDataReference player { get { return instance._playerData; } }
}
