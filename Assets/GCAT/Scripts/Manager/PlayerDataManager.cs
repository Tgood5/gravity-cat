﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


//* Use 'using' statements for classes that implement IDisposable(e.g.FileStream, BinaryFormatter etc.)
//* Use System.IO.Path.Combine to combine paths safely.Paths are constructed differently on different OS.This avoids a lot of potential problems.
public class PlayerDataManager : Singleton<PlayerDataManager>
{
    public string currentFileName;

    protected override void InitSingletonInstance()
    {
        
    }

    private void Start()
    {
        // testing
        if (!string.IsNullOrEmpty(currentFileName))
        {
            LoadGame(currentFileName);
        }
        else
        {
            // create new save
            currentFileName = "testData";
            SaveGame();
            LoadGame(currentFileName);
        }
    }

    public static void SaveGame()
    {
        string path = Path.Combine(Application.persistentDataPath, instance.currentFileName+".gcat");
        // 'using' will close filestream automatically, even if serialization fails
        using (FileStream stream = new FileStream(path, FileMode.Create))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            
            PlayerData saveData = new PlayerData();
            instance.SetValuesToData(saveData);

            formatter.Serialize(stream, saveData);
        }           
    }

    public static void LoadGame(string fileName)
    {
        string path = Path.Combine(Application.persistentDataPath, fileName+".gcat");
        if (File.Exists(path))
        {          
            // 'using' to prevent open file leaks if deserialize fails
            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                PlayerData data = formatter.Deserialize(stream) as PlayerData;

                // set values
                instance.LoadValuesFromData(data);
                instance.currentFileName = fileName;
            }                        
        }
        else
        {
            Debug.LogError("save file not found! " + path);
        }
    }

    // PRIVATE HELPER

    // set playerdata values using current gamestate
    private void SetValuesToData(PlayerData data)
    {
        AssetData.player.SetValuesToData(data);
    }

    private void LoadValuesFromData(PlayerData data)
    {
        AssetData.player.LoadValuesFromData(data);       
    }

}
