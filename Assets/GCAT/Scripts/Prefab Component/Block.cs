﻿using ScriptableObjectArchitecture;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public FloatReference fallSpeed;
    public Vector3 direction;
    public Vector3 spawnpoint;
    public float traveldist;
    public GameObjectCollection allBlocks;

    protected virtual void Awake()
    {
        spawnpoint = transform.position;
        allBlocks.Add(gameObject);
    }

    protected virtual void FixedUpdate()
    {
        transform.Translate(direction * fallSpeed.Value * Time.deltaTime);                
    }

    private void OnDestroy()
    {
        allBlocks.Remove(gameObject);
    }
}
