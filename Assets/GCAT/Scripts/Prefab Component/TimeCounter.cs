﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// keeps track of elapsed time, counts minutes/seconds 
public class TimeCounter : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI displayText = null;

    public float totalTime;

    public int minutes = 0;
    public int seconds = 0;

    public float secondTime = 1f; // length of one second
    float elapsed = 0f;

    public bool countDown;

    private void Start()
    {
        SetDisplay();
    }

    void Update()
    {
        if (countDown)
            CountDown();
        else
            CountUp();
    }

    void CountUp()
    {
        elapsed += Time.deltaTime;
        if(elapsed >= secondTime)
        {
            elapsed = 0;
            seconds++;
            totalTime++;
            if(seconds >= 60)
            {
                seconds = 0;
                minutes++;
            }
            SetDisplay();
        }
    }

    void CountDown()
    {
        elapsed += Time.deltaTime;
        if (elapsed >= secondTime)
        {
            elapsed = 0;
            seconds--;
            totalTime++;
            if (seconds < 0)
            {
                seconds = 59;
                minutes--;
            }
            SetDisplay();
        }
    }

    public void SetDisplay()
    {
        if(displayText != null)
        {
            string min = minutes.ToString();
            string sec = seconds.ToString();
            if (seconds < 10)
                sec = "0" + sec;

            displayText.text = string.Format("{0}:{1}", min, sec);
        }       
    }
}
