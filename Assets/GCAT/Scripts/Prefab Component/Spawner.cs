﻿using ScriptableObjectArchitecture;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spawner : MonoBehaviour
{
    [SerializeField] bool isSpawning;
    public bool spawning { get { return isSpawning; } set { prevSpawnTime = Time.time; isSpawning = value; } }

    protected float prevSpawnTime;
    public FloatReference timerRef;      

    [SerializeField] protected List<Transform> spawnPoints;
    public List<GameObject> prefabList;

    void Update()
    {
        OnUpdate();
    }

    protected void OnUpdate()
    {
        if (Time.time - prevSpawnTime > timerRef.Value)
        {
            if (spawning)
                SpawnRandom();
            prevSpawnTime = Time.time;
        }
    }

    public abstract GameObject SpawnObject(GameObject toSpawn, Vector3 pos);
    public abstract GameObject SpawnRandom();
}
