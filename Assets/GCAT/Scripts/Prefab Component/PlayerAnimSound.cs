﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimSound : MonoBehaviour
{
    public void OnFootStep()
    {
        SoundManager.PlaySound(soundFx.step);
    }
}
