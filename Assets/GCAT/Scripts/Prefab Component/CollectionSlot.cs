﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CollectionSlot : Selectable
{
    public CollectionScreen collection;

    public Image icon;
    public Image border;
    public Image lockImage;

    public Unlockable unlockable;

    void Confirm()
    {
        this.PostNotification(CollectionScreen.selectItemNotification, unlockable);
    }

    public override void Select()
    {
        Debug.Log("slot selected!");
        collection.SelectUI(this);
        base.Select();
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        Select();
        Confirm(); // test
        base.OnPointerDown(eventData);
    }

    
}
