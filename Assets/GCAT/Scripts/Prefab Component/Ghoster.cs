﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghoster : MonoBehaviour
{
    public float ghostDelay = 0.2f;
    public float fadeTime = 0.5f;
    float lastSpawnTime;
    public SpriteRenderer spriter;

    public Ghost[] ghosts;
    public int ghostIndex;

    // Start is called before the first frame update
    void Start()
    {
        spriter = GetComponentInParent<SpriteRenderer>();
        ghosts = GetComponentsInChildren<Ghost>();

        foreach(Ghost ghost in ghosts)
        {
            ghost.fadeTime = fadeTime;
            ghost.transform.SetParent(null);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > lastSpawnTime + ghostDelay)
        {
            ghosts[ghostIndex].SpawnGhost(spriter.sprite, transform.position, transform.rotation);
            lastSpawnTime = Time.time;
            ghostIndex++;
            if (ghostIndex >= ghosts.Length)
                ghostIndex = 0;
        }
    }
}
