﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : Spawner
{
    [SerializeField]BoxCollider2D screenArea = null;

    void Update()
    {
        OnUpdate();
    }
    public override GameObject SpawnObject(GameObject toSpawn, Vector3 pos)
    {
        ScreenWrapper coin = Instantiate(toSpawn, pos, transform.rotation).GetComponent<ScreenWrapper>();
        if (screenArea != null)
        {
            coin.screenArea = screenArea;
            coin.AdjustClonesToArea();
        }
        return coin.gameObject;
    }
    public override GameObject SpawnRandom()
    {
        int spawnIndex = Random.Range(0, spawnPoints.Count);
        int prefabIndex = Random.Range(0, prefabList.Count);

        Transform spawnPoint = spawnPoints[spawnIndex];
        GameObject toSpawn = prefabList[prefabIndex];
       
        return SpawnObject(toSpawn, new Vector3(spawnPoint.position.x, spawnPoint.position.y, spawnPoint.position.z));
    }
}
