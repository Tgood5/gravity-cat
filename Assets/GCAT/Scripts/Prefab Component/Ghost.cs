﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    SpriteRenderer sr;
    public Color startColor;
    public Color endColor;
    public float fadeTime = 0.2f;
    float currentFadeTime = 99f;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        currentFadeTime = fadeTime; 
    }

    private void Update()
    {
        if(currentFadeTime < fadeTime)
        {
            sr.color = Color.Lerp(startColor, endColor, currentFadeTime/fadeTime);
            currentFadeTime += Time.deltaTime;
        }       
    }

    public void SpawnGhost(Sprite sprite, Vector3 position, Quaternion rotation)
    {
        sr.sprite = sprite;
        transform.SetPositionAndRotation(position, rotation);
        currentFadeTime = 0;
    }
}
