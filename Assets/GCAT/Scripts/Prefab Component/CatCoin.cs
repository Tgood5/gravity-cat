﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatCoin : MonoBehaviour
{
    public const string CoinCollectNotification = "CoinCollectedNotification";  

    [SerializeField] Animator anim = null;
    [SerializeField] Rigidbody2D body = null;
    [SerializeField] Gravitation gravitation = null;

    //public Vector3 direction;
    //public float fixedSpeed = 0.0f;

    public float bounceforce;
    public bool collected = false;
    public bool sticky = false;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collected)
        {
            if (sticky)
            {
                transform.SetParent(collision.gameObject.transform);
                body.gravityScale = 0;
            }
            else 
                body.AddForce((Vector2.up) * bounceforce, ForceMode2D.Impulse);
        }
                 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collected)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                collected = true;
                StartCoroutine(OnCollect());
            }
            if (collision.gameObject.CompareTag("harmful"))
                Destroy(gameObject);
            //else
            //    SoundManager.PlaySound(soundFx.clink); too common and annoying!
        }       
    }

    IEnumerator OnCollect()
    {
        body.simulated = false;
        SoundManager.PlaySound(soundFx.coin);
        anim.SetTrigger("collect");
        this.PostNotification(CoinCollectNotification);

        yield return new WaitForSeconds(3f);

        Destroy(gameObject);
    }
}
