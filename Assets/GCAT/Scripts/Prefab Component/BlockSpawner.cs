﻿using ScriptableObjectArchitecture;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : Spawner
{
    int lastSpawnIndex;
    int lastObjectIndex;
    public int blockLimit = 12;
    public Queue<Block> spawnQueue = new Queue<Block>();

    public Vector3 launchDir = Vector3.down;
    public FloatReference speedRef;

    void FixedUpdate()
    {
        OnUpdate();
    }

    public override GameObject SpawnObject(GameObject toSpawn, Vector3 pos)
    {
        // TODO: pooling
        Block newblok = Instantiate(toSpawn.gameObject, pos, transform.rotation).GetComponent<Block>();
        if (newblok)
        {
            newblok.fallSpeed = speedRef;
            newblok.direction = launchDir;
            spawnQueue.Enqueue(newblok);
        }       
        if(spawnQueue.Count > blockLimit)
        {
            Block toRemove = spawnQueue.Dequeue();
            Destroy(toRemove.gameObject);                   
        }
        return newblok.gameObject;
    }

    public override GameObject SpawnRandom()
    {
        int spawnIndex;
        do {
            spawnIndex = Random.Range(0, spawnPoints.Count);
        } while (spawnIndex == lastSpawnIndex);
        lastSpawnIndex = spawnIndex;

        int blockIndex;
        do {
            blockIndex = Random.Range(0, prefabList.Count);
        } while (blockIndex == lastObjectIndex);
        lastObjectIndex = blockIndex;
      
        return SpawnObject(prefabList[blockIndex], spawnPoints[spawnIndex].position);      
    }
}
