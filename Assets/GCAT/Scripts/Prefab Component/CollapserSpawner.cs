﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ScriptableObjectArchitecture;

public class CollapserSpawner : Spawner
{
    int lastSpawnIndex = -1;
    int lastObjectIndex = -1;
    public int blockLimit = 12;
    public Queue<CollapsingBlock> spawnQueue = new Queue<CollapsingBlock>();

    public Vector3 launchDir = Vector3.down;
    public FloatReference speedRef;
    public FloatReference collapseTimeRef;

    void FixedUpdate()
    {
        OnUpdate();
    }

    public override GameObject SpawnObject(GameObject toSpawn, Vector3 pos)
    {
        // TODO: pooling
        CollapsingBlock newblok = Instantiate(toSpawn.gameObject, pos, transform.rotation).GetComponent<CollapsingBlock>();
        if (newblok)
        {
            //newblok.gameObject.SetActive(true);
            newblok.fallSpeed = speedRef;
            newblok.direction = launchDir;
            newblok.timeToCollapse = collapseTimeRef;
            spawnQueue.Enqueue(newblok);
        }       
        if(spawnQueue.Count > blockLimit)
        {
            Block toRemove = spawnQueue.Dequeue();
            //foreach(Block child in transform.GetComponentsInChildren<Block>())
            //    child.transform.SetParent(null);
            Destroy(toRemove.gameObject);                   
        }
        return newblok.gameObject;
    }

    public override GameObject SpawnRandom()
    {
        int spawnPointIndex;
        do {
            spawnPointIndex = Random.Range(0, spawnPoints.Count);
        } while (spawnPointIndex == lastSpawnIndex && spawnPoints.Count > 1);
        lastSpawnIndex = spawnPointIndex;

        int objectIndex;
        do {
            objectIndex = Random.Range(0, prefabList.Count);
        } while (objectIndex == lastObjectIndex);
        lastObjectIndex = objectIndex;
      
        return SpawnObject(prefabList[objectIndex], spawnPoints[spawnPointIndex].position);       
    }
}
