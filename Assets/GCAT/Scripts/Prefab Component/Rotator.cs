﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float speed = 1f;
    public bool clockwise = true;

    public bool randomStartSpeed = true;
    float lowSpeed = 0.25f;
    float hiSpeed = .9f;

    public Rigidbody2D body;

    private void Start()
    {
        if (randomStartSpeed)
            speed = Random.Range(lowSpeed, hiSpeed);
    }
    void FixedUpdate()
    {
        Rotate();
    }

    void Rotate()
    {
        //if(clockwise)
        //    transform.Rotate(new Vector3(0, 0, -speed));
        //else
        //    transform.Rotate(new Vector3(0, 0, speed));

        body.MoveRotation(transform.eulerAngles.z + speed);
    }
}
