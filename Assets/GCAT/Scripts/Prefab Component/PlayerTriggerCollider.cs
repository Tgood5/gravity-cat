using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class PlayerTriggerCollider : MonoBehaviour
{
    public const string PlayerTriggerNotification = "playerTriggerCollider.Notification";

    Collider2D c2 { get { return GetComponent<Collider2D>(); } }

    [SerializeField] bool disableOnTrigger = true;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == 14)
        {
            this.PostNotification(PlayerTriggerNotification);
            if(disableOnTrigger)
                c2.enabled = false;
        }
    }
}
