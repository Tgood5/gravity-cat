using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using ScriptableObjectArchitecture;

public class CollapsingBlock : Block
{
    public const string BlockCollapseNotification = "blockCollapsed";

    public FloatReference timeToCollapse;
    public int collisionLimit = 1;
    int collisionCount = 0;
    bool collapsed = false;
    bool falling = false;

    [SerializeField] Animator animator;
    [SerializeField] Rigidbody2D body;
    [SerializeField] Collider2D collapseTrigger;

    private void Start()
    {
        StartCoroutine(SpawnAnimation());
    }

    private void FixedUpdate()
    {
        if (falling && fallSpeed != null)
        {
            transform.Translate(direction * fallSpeed.Value * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collapsed)
        {
            if (collision.gameObject.layer == 14) // make sure collision is player
            {
                //if (collision.GetContact(0).collider == collapseTrigger) // make sure player landed on top area
                {
                    collisionCount++;
                    if (collisionCount >= collisionLimit)
                    {
                        collapsed = true;
                        this.PostNotification(BlockCollapseNotification);
                        StartCoroutine(Collapse());
                    }
                }

            }
        }
    }

    public IEnumerator Collapse()
    {
        // shake the block until time to collapse
        float elapsed = 0.0f;
        while (elapsed < timeToCollapse.Value)
        {
            animator.SetFloat("shake", elapsed/timeToCollapse.Value);
            elapsed += Time.deltaTime;
            yield return null;
        }
        animator.SetFloat("shake", 1.0f);

        // collapse the block here
        falling = true;
        SoundManager.PlaySound(soundFx.rumble);
    }

    public IEnumerator SpawnAnimation()
    {
        Vector3 endpos = transform.position;
        transform.Translate(direction * -19); // todo screen size?
        Vector3 startpos = transform.position;

        float duration = 2f; // todo ??
        float elapsed = 0f;

        //SoundManager.PlaySound(soundFx.rumble);
        while (elapsed <= duration)
        {
            elapsed += Time.deltaTime;
            transform.position = Vector3.Lerp(startpos, endpos, EaseOut(elapsed / duration));
            
            yield return null;
        }
        transform.position = endpos;
    }

    // used for lerp easing
    float flip(float x)
    {
        return 1 - x;
    }

    float EaseOut(float x)
    {
        return flip(flip(x) * flip(x));
    }
}
