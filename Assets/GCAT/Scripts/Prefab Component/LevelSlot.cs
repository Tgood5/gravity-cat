using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LevelSlot : Selectable
{
    public Image icon;
    public Image border;
    public Image lockImage;
    public int levelSceneIndex;
    public string description;
    protected override void Start()
    {
        
    }
    public override void OnPointerDown(PointerEventData eventData)
    {
        Select();
        this.PostNotification(LevelSelectScreen.selectLevelNotification, levelSceneIndex);
        base.OnPointerDown(eventData);
    }
}
