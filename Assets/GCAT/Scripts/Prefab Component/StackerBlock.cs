﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackerBlock : Block
{
    public const string blockLandingNotification = "stackerBlock.LandedNote";

    public int height = 2; //effective height in units
    public bool landed;
    Rigidbody2D body;

    protected override void Awake()
    {
        base.Awake();
        if (body == null)
        {
            body = GetComponentInChildren<Rigidbody2D>();
        }
    }

    protected override void FixedUpdate()
    {
        if (!landed) 
        {
           base.FixedUpdate();
        }          
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(!landed)
            if (collision.gameObject.layer == 15)
            {                     
                landed = true;
                body.isKinematic = true;
                // snap to unit position
                transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), transform.position.z);
    
                 this.transform.PostNotification(blockLandingNotification, transform.position + (Vector3.up * (height)));          
            }           
    }
}
